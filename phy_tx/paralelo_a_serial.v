////////////////////////////////////////////////////////////////////////////////
//Definición módulo conversor paralelo a serial 
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module paralelo_a_serial (
    input clk,
    input reset_L,
    input [7:0] data_in,
    input valid_data_in,
    output reg [1:0] data_out); //Declaración de variables entrada-salida
    
    //Definición variables internas
    reg [1:0] contador;
    reg [1:0] temporal;
    reg [7:0] data_valid;

    //Bloque de lógica de salida - lógica secuencial.
    always @(posedge clk)
    begin
        if(reset_L)
        begin
            data_out[1:0] <= temporal[1:0];
            contador <= contador+1;
            if(valid_data_in)
            begin
                data_valid[7:0] <= data_in[7:0];
            end
            else
            begin
                data_valid[7:0] <= 8'hBC;
            end
        end
        else
        begin
            data_out[1:0] <= 2'b0;
            contador <= 0;
        end
    end

    //Bloque de lógica interna - lógica combinacional.
    always @(*)
    begin
	case (contador)
	    0: temporal[1:0] = data_valid[1:0];
	    1: temporal[1:0] = data_valid[3:2];
	    2: temporal[1:0] = data_valid[5:4];
	    default: temporal[1:0] = data_valid[7:6];
	endcase
    end

endmodule
