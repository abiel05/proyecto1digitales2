`timescale 1ns /100ps
module BancoPruebas_s2p();
    wire clk, valid_data_out, reset_L;
    wire [1:0] data_in;
    wire [7:0] data_out;

    probador_s2p p0 (.clk(clk),
	    .reset_L(reset_L),
	    .data_in(data_in[1:0]),
	    .valid_data_out(valid_data_out),
	    .data_out(data_out[7:0]));

    serial_a_paralelo s2p_0 (.clk(clk),
	    .reset_L(reset_L),
	    .data_in(data_in[1:0]),
	    .valid_data_out(valid_data_out),
	    .data_out(data_out[7:0]));
    
endmodule
