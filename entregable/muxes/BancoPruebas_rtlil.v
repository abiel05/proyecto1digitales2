////////////////////////////////////////////////////////////////////////////////
//Definición módulo de Banco de Pruebas
////////////////////////////////////////////////////////////////////////////////
`include "mux_estructural_auto_mapped.v"
`include "probador_rtlil.v"
`include "greloj_cond.v"
`include "cmos_cells.v"
`include "muxL1.v"
`timescale 1ns /100ps
module BancoPruebas();

    //Declaración de variables internas
    wire valid_data_0,valid_data_1,valid_data_2,valid_data_3,valid_data_out_cond_0,valid_data_out_est_0,valid_data_out_cond_1,valid_data_out_est_1;
    wire [7:0] data_0, data_1, data_2, data_3, data_out_cond_0, data_out_cond_1, data_out_est_0, data_out_est_1;

    wire clk_f, clk_2f, clk_4f, clk_16f;
    wire reset_L, reset_clk;
    //Instanciando módulos Reloj y Probador.
    greloj_cond clks (/*autoinst*/
    .clk_8                          (clk_8f                                      ), // output
    .clk_4                          (clk_4f                                      ), // output
    .clk_2                          (clk_2f                                      ), // output
    .clk_1                          (clk_f                                      ), // output
    .reset_L                        (reset_clk                                     ), // input  // INST_NEW
    .clk_in                         (clk_16f                                     )  // input 

    // Salida de 4 ciclos reloj
    // 16 / 4 = 4 ciclos reloj
);
    probador t1(.clk(clk_16f),
            .reset_L(reset_L),
            .reset_clk(reset_clk),
            .data_0(data_0),
            .valid_data_0(valid_data_0),
            .data_1(data_1),
            .valid_data_1(valid_data_1),
            .data_2(data_2),
            .valid_data_2(valid_data_2),
            .data_3(data_3),
            .valid_data_3(valid_data_3),
            .data_out_cond_0(data_out_cond_0),
            .valid_data_out_cond_0(valid_data_out_cond_0),
            .data_out_cond_1(data_out_cond_1),
            .valid_data_out_cond_1(valid_data_out_cond_1),
            .data_out_est_0(data_out_est_0),
            .valid_data_out_est_0(valid_data_out_est_0),
            .data_out_est_1(data_out_est_1),
            .valid_data_out_est_1(valid_data_out_est_1));

    //Instanciando módulo Multiplexor 4:2 con selector automático. Diseño conductual
    muxL1 muxL1_cond_0 (
	        .clk_f(clk_f),
	        .clk_2f(clk_2f),
            .reset_L(reset_L),
            .data_0(data_0),
            .valid_data_0(valid_data_0),
            .data_1(data_1),
            .valid_data_1(valid_data_1),
            .data_2(data_2),
            .valid_data_2(valid_data_2),
            .data_3(data_3),
            .valid_data_3(valid_data_3),
            .data_out_0(data_out_cond_0),
            .valid_data_out_0(valid_data_out_cond_0),
            .data_out_1(data_out_cond_1),
            .valid_data_out_1(valid_data_out_cond_1));

    //Instanciando módulo Multiplexor 4:2 con selector automático. Diseño estructural Mapeado
    muxL1_estructural_mapped muxL1_est_0 (
	        .clk_f(clk_f),
	        .clk_2f(clk_2f),
            .reset_L(reset_L),
            .data_0(data_0),
            .valid_data_0(valid_data_0),
            .data_1(data_1),
            .valid_data_1(valid_data_1),
            .data_2(data_2),
            .valid_data_2(valid_data_2),
            .data_3(data_3),
            .valid_data_3(valid_data_3),
            .data_out_0(data_out_est_0),
            .valid_data_out_0(valid_data_out_est_0),
            .data_out_1(data_out_est_1),
            .valid_data_out_1(valid_data_out_est_1));

endmodule
