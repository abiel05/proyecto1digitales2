////////////////////////////////////////////////////////////////////////////////
//Definición modulo de verificación
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module probador (input clk,
                 input [7:0] data_out,
                 input [7:0] data_out_estructural,
                 input valid_data_out,
                 input valid_data_out_est,
                 output reg valid_data_0,
                 output reg valid_data_1,
                 output reg valid_data_2,
                 output reg valid_data_3,
                 output reg reset_L,
                 output reg [7:0] data_0,
                 output reg [7:0] data_1,
                 output reg [7:0] data_2,
                 output reg [7:0] data_3);//Declaración de variables entrada-salida

    //Declaración de variables internas
    reg compare;
    reg [7:0] conductual, estructural;
    integer transiciones, transiciones_est, transiciones0, transiciones1, transiciones2, transiciones3, transiciones_est0, transiciones_est1, transiciones_est2, transiciones_est3;
    real factor_potencia;

    //Inicialización de registros de salida
    initial
    begin
        reset_L=0;
        data_0[7:0]=0;
        data_1[7:0]=0;
        data_2[7:0]=0;
        data_3[7:0]=0;
        valid_data_0=0;
        valid_data_1=0;
        valid_data_2=0;
        valid_data_3=0;
        compare=0;
        conductual[7:0]=8'b0;
        estructural[7:0]=8'b0;
        transiciones=0;
        transiciones0=0;
        transiciones1=0;
        transiciones2=0;
        transiciones3=0;
        transiciones_est=0;
        transiciones_est0=0;
        transiciones_est1=0;
        transiciones_est2=0;
        transiciones_est3=0;
        factor_potencia = 0;
    end

    //Asignación registros de salida
    always @(*)
    begin
        $dumpfile("senalesVCD_mapped_L1.vcd");
        $dumpvars;
        $display ("Inicio Simulación!!");
        $monitor ($time,"\tclk = %b, reset_L = %b, data_0 = %h, data_1 = %h, data_out = %h, data_out_est = %h", clk, reset_L, data_0, data_1, data_out, data_out_estructural);
        #5000 reset_L = 1;
        #4000 data_0[7:0] = 8'h8;
        data_2[7:0] = 8'hE;
        data_3[7:0] = 8'h6;
        #2000 valid_data_0 = 1;
        valid_data_2 = 1;
        #2000 valid_data_0 = 0;
        #2000 valid_data_0 = 1;
        valid_data_3 = 1;
        data_1[7:0] = 8'hF;
        #2000 valid_data_1 = 1;
        #2000 valid_data_1 = 0;
        #2000 valid_data_1 = 1;
        #10000 data_0[7:0] = 8'hE;
        data_1[7:0] = 8'h7;
        data_2[7:0] = 8'h3;
        data_3[7:0] = 8'hA;
        #20000 reset_L = 0;
        #5000 data_0[7:0] = 8'hA;
        data_1[7:0] = 8'h5;
        #5000 reset_L = 1;
        #10000 data_0[7:0] = 8'h6;
        data_1[7:0] = 8'hC;
        #2000 valid_data_1 = 1;
        #2000 valid_data_1 = 0;
        #2000 valid_data_1 = 1;
        #2000 valid_data_0 = 1;
        #2000 valid_data_0 = 0;
        #2000 valid_data_0 = 1;
        #20000 $finish;
    end
    
    //Asignación de salidas de los multiplexores a registros internos de probador.v para comparación
    always @(posedge clk)
    begin
        conductual[7:0] <= data_out[7:0];
        estructural[7:0] <= data_out_estructural[7:0];
    end
    //Lógica de comparación
    always @(*)
    begin
        compare = 0;
        if(conductual != estructural)
        begin
            compare = 1;
        end
    end

    //Lógica de cálculo de consumo de potencia - Contadores de transiciones positivas en las salidas
    always @(posedge data_out[0])
    begin
        transiciones0<=transiciones0+1;
    end
    always @(posedge data_out[1])
    begin
        transiciones1<=transiciones1+1;
    end
    always @(posedge data_out[2])
    begin
        transiciones2<=transiciones2+1;
    end
    always @(posedge data_out[3])
    begin
        transiciones3<=transiciones3+1;
    end

    always @(posedge data_out_estructural[0])
    begin
        transiciones_est0<=transiciones_est0+1;
    end
    always @(posedge data_out_estructural[1])
    begin
        transiciones_est1<=transiciones_est1+1;
    end
    always @(posedge data_out_estructural[2])
    begin
        transiciones_est2<=transiciones_est2+1;
    end
    always @(posedge data_out_estructural[3])
    begin
        transiciones_est3<=transiciones_est3+1;
    end

    always @(*) 
    begin
        transiciones = transiciones0+transiciones1+transiciones2+transiciones3;
        transiciones_est = transiciones_est0+transiciones_est1+transiciones_est2+transiciones_est3;
        //factor_potencia = (transiciones/tiempo_simulación)/(1/periodo_reloj);
    end    

endmodule
