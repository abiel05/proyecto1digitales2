////////////////////////////////////////////////////////////////////////////////
//Definicion modulo receptor
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
`include "s2p_conductual.v"
`include "demuxL1.v"
module phy_rx (
    input clk_1,
    input clk_2,
    input clk_4,
    input clk_16,
    input reset_L,
    input [1:0] data_in,
    output reg [7:0] data_out_0,
    output reg [7:0] data_out_1,
    output reg [7:0] data_out_2,
    output reg [7:0] data_out_3,
    output reg valid_data_out_0,
    output reg valid_data_out_1,
    output reg valid_data_out_2,
    output reg valid_data_out_3);
    
    
    reg [7:0] data_in_dmL1_0, data_in_dmL1_1;
    reg valid_data_in_dmL1_0, valid_data_in_dmL1_1;

    wire [7:0] data_out_dmL2_0, data_out_dmL2_1;
    wire valid_data_out_dmL2_0, valid_data_out_dmL2_1;

    wire [7:0] data_out_dmL1_0, data_out_dmL1_1, data_out_dmL1_2, data_out_dmL1_3;
    wire valid_data_out_dmL1_0, valid_data_out_dmL1_1, valid_data_out_dmL1_2, valid_data_out_dmL1_3;

    wire[7:0] data_out_s2p;
    wire valid_data_out_s2p;

    //modulo s2p
    s2p_conductual s2p (/*AUTOINST*/
			// Outputs
			.valid_data_out	(valid_data_out_s2p),
			.data_out	(data_out_s2p[7:0]),
			// Inputs
			.clk		(clk_16),  //para que trabaje a 16 ciclos, la mas rapida
			.reset_L	(reset_L),
			.data_in	(data_in[1:0]));
    
    //modulo L2
    demuxL2 dL2 (/*AUTOINST*/
        .clk_f0		        	(clk_2),
        .clk_2f0		        	(clk_4),
        .reset_L		        (reset_L),
        .data_out_0             (data_out_dmL2_0[7:0]                            ), // output // INST_NEW
        .valid_data_out_0       (valid_data_out_dmL2_0                           ), // output // INST_NEW
        .valid_data_out_1       (valid_data_out_dmL2_1                           ), // output // INST_NEW
        .data_out_1             (data_out_dmL2_1[7:0]                            ), // output // INST_NEW
        .valid_data_in          (valid_data_out_s2p                              ), // input  // INST_NEW
        .data_in                (data_out_s2p[7:0]                               )); // input  // INST_NEW


    always @ (posedge clk_2)  //transicion de L2 a L1 es a 2f
    begin
        data_in_dmL1_0 <= data_out_dmL2_0;
        valid_data_in_dmL1_0 <= valid_data_out_dmL2_0;
        valid_data_in_dmL1_1 <= valid_data_out_dmL2_1;
        data_in_dmL1_1 <= data_out_dmL2_1;
    end

    //modulo L1
    demuxL1 dL1 (/*AUTOINST*/
        .clk_f1		        	(clk_1),
        .clk_2f1		        	(clk_2),
        .reset_L	                	(reset_L),
        .data_in_dmL1_0                 (data_in_dmL1_0[7:0]                        ), // input  // INST_NEW
        .valid_data_in_dmL1_0           (valid_data_in_dmL1_0                       ), // input  // INST_NEW
        .data_in_dmL1_1                 (data_in_dmL1_1[7:0]                        ), // input  // INST_NEW
        .valid_data_in_dmL1_1           (valid_data_in_dmL1_1                       ), // input  // INST_NEW
        .data_out_dmL1_0                (data_out_dmL1_0[7:0]                       ), // output // INST_NEW
        .valid_data_out_dmL1_0          (valid_data_out_dmL1_0                      ), // output // INST_NEW
        .data_out_dmL1_1                (data_out_dmL1_1[7:0]                       ), // output // INST_NEW
        .valid_data_out_dmL1_1          (valid_data_out_dmL1_1                      ), // output // INST_NEW
        .data_out_dmL1_2                (data_out_dmL1_2[7:0]                       ), // output // INST_NEW
        .valid_data_out_dmL1_2          (valid_data_out_dmL1_2                      ), // output // INST_NEW
        .data_out_dmL1_3                (data_out_dmL1_3[7:0]                       ), // output // INST_NEW
        .valid_data_out_dmL1_3          (valid_data_out_dmL1_3                      )); // output // INST_NEW

    always @ (posedge clk_1)  //transicion de L1 a salida es a 1f
    begin
        data_out_0 <= data_out_dmL1_0;
        valid_data_out_0 <= valid_data_out_dmL1_0;
        data_out_1 <= data_out_dmL1_1;
        valid_data_out_1 <= valid_data_out_dmL1_1;
        data_out_2 <= data_out_dmL1_2;
        valid_data_out_2 <= valid_data_out_dmL1_2;
        data_out_3 <= data_out_dmL1_3;
        valid_data_out_3 <= valid_data_out_dmL1_3;
    end


endmodule
