////////////////////////////////////////////////////////////////////////////////
//Definición módulo demux 4:2 con selector automático
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
`include "demuxL2.v"
module demuxL1 (
    input clk_f1,
    input clk_2f1,
    input reset_L,
    input [7:0] data_in_dmL1_0,
    input valid_data_in_dmL1_0,
    input [7:0] data_in_dmL1_1,            
    input valid_data_in_dmL1_1,
    output [7:0] data_out_dmL1_0,
    output valid_data_out_dmL1_0,
    output [7:0] data_out_dmL1_1,
    output valid_data_out_dmL1_1,
    output [7:0] data_out_dmL1_2,
    output valid_data_out_dmL1_2,
    output [7:0] data_out_dmL1_3,
    output valid_data_out_dmL1_3);//Declaración de variables entrada-salida

    //Instanciando 2 módulos demux 2:1 con selector automático para formar 1 demux 4:2 con selector automático.
    demuxL2 demux_0(/*autoinst*/
    .clk_f0                            (clk_f1                                        ), // input 
    .clk_2f0                            (clk_2f1                                        ), // input 
    .reset_L                        (reset_L                                    ), // input 
    .data_out_0                     (data_out_dmL1_0[7:0]                            ), // output
    .valid_data_out_0               (valid_data_out_dmL1_0                           ), // output
    .valid_data_out_1               (valid_data_out_dmL1_1                           ), // output
    .data_out_1                     (data_out_dmL1_1[7:0]                            ), // output
    .valid_data_in                  (valid_data_in_dmL1_0                              ), // input 
    .data_in                        (data_in_dmL1_0[7:0]                               )  // input 

        //Declaración de variables internas

        //Lógica de salida data_out_0 y data_out_1
);


    demuxL2 demux_1(/*autoinst*/
    .clk_f0                            (clk_f1                                        ), // input 
    .clk_2f0                            (clk_2f1                                        ), // input 
    .reset_L                        (reset_L                                    ), // input 
    .data_out_0                     (data_out_dmL1_2[7:0]                            ), // output
    .valid_data_out_0               (valid_data_out_dmL1_2                           ), // output
    .valid_data_out_1               (valid_data_out_dmL1_3                           ), // output
    .data_out_1                     (data_out_dmL1_3[7:0]                            ), // output
    .valid_data_in                  (valid_data_in_dmL1_1                              ), // input 
    .data_in                        (data_in_dmL1_1[7:0]                               )  // input 

        //Declaración de variables internas

        //Lógica de salida data_out_0 y data_out_1
);

endmodule
