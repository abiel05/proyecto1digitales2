////////////////////////////////////////////////////////////////////////////////
//Definición modulo de verificación
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module probador_p2s (input [1:0] data_out_cond,
    input [1:0] data_out_est,
    output reg clk,
    output reg reset_L,
    output reg [7:0] data_in,
    output reg valid_data_in);


    initial
    begin
		data_in[7:0]=8'h0;
		valid_data_in = 0;
		clk = 0;
		reset_L = 0;
    end

    //Declaración de variables internas
    reg compare;
    reg [1:0] conductual_in, conductual, estructural;

    //Reloj base a 16f
    always
    begin
	#2 clk = ~clk;
    end

    //Asignación registros de salida
    always @(*)
    begin
        $dumpfile("p2s.vcd");
        $dumpvars;
        $display ("Inicio Simulación!!");
        $monitor ($time,"\tclk = %b, data_in = %h, data_out_cond = %h, data_out_est = %h", clk, data_in, data_out_cond, data_out_est);
        #10 valid_data_in = 1;
            data_in[7:0] = 8'hFF;
        #16 reset_L = 1'b1;
        #16 data_in[7:0] = 8'h00;
        #16 data_in[7:0] = 8'hA8;
        #16 data_in[7:0] = 8'hDD;
	    #16 valid_data_in = 0;
            data_in[7:0] = 8'hDD;
        #16 data_in[7:0] = 8'hE1;
        #16 data_in[7:0] = 8'h05;
	    #16 valid_data_in = 1;
            data_in[7:0] = 8'h04;
        #16 data_in[7:0] = 8'h45;
        #20 $finish;
    end

    //Asignación de salidas de los multiplexores a registros internos de probador.v para comparación
    always @(posedge clk)
    begin
        conductual_in[1:0] <= data_out_cond[1:0];
        conductual[1:0] <= conductual_in[1:0];
        estructural[1:0] <= data_out_est[1:0];
    end
    //Lógica de comparación
    always @(*)
    begin
        compare = 0;
        if(conductual != estructural)
        begin
            compare = 1;
        end
    end

endmodule
