`timescale 1ns /100ps
module BUF(A, Y);
input A;
output Y;
assign Y = A;
endmodule

module NOT(A, Y);
input A;
output Y;
assign #(0.3:1.1:2.3) Y = ~A;
endmodule

module NAND(A, B, Y);
input A, B;
output Y;
assign #(0.5:0.9:1.6) Y = ~(A & B);
endmodule

module NOR(A, B, Y);
input A, B;
output Y;
assign #(0.4:1.0:1.9) Y = ~(A | B);
endmodule

module DFF(C, D, Q);
input C, D;
output reg Q;
wire notifier_setup, notifier_hold;
wire notifier_setup2, notifier_hold2;
INSTR_setup #(0.7,"pos") su1(D, C, notifier_setup);
INSTR_hold #(0.0,"pos") hold1(C, D, notifier_hold);
INSTR_setup #(2.0,"pos") su2(D, C, notifier_setup2);
INSTR_hold #(2.0,"pos") hold2(clk, D, notifier_hold2);
always @(posedge C)
	Q <= D;
endmodule

module DFFSR(C, D, Q, S, R);
input C, D, S, R;
output reg Q;
always @(posedge C, posedge S, posedge R)
	if (S)
		Q <= 1'b1;
	else if (R)
		Q <= 1'b0;
	else
		Q <= D;
endmodule
