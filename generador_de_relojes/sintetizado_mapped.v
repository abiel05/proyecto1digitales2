/* Generated by Yosys 0.7 (git sha1 61f6811, gcc 6.2.0-10 -O2 -fdebug-prefix-map=/build/yosys-kuc41c/yosys-0.7=. -fstack-protector-strong -fPIC -Os) */

module greloj_cond(clk_4, clk_2, clk_1, reset_L, clk_16);
  wire _000_;
  wire _001_;
  wire _002_;
  wire [2:0] _003_;
  wire [1:0] _004_;
  wire _005_;
  wire _006_;
  wire _007_;
  wire _008_;
  wire _009_;
  wire _010_;
  wire _011_;
  wire _012_;
  wire _013_;
  wire _014_;
  wire _015_;
  wire _016_;
  wire _017_;
  wire _018_;
  wire _019_;
  wire _020_;
  wire _021_;
  wire _022_;
  wire _023_;
  wire _024_;
  wire _025_;
  wire _026_;
  wire _027_;
  wire _028_;
  wire _029_;
  wire _030_;
  wire _031_;
  wire _032_;
  wire _033_;
  wire _034_;
  wire _035_;
  wire _036_;
  wire _037_;
  wire _038_;
  wire _039_;
  wire _040_;
  wire _041_;
  wire _042_;
  wire _043_;
  wire _044_;
  wire _045_;
  wire _046_;
  wire [1:0] _047_;
  wire [2:0] _048_;
  wire _049_;
  wire [2:0] _050_;
  wire [1:0] _051_;
  wire _052_;
  wire _053_;
  wire [31:0] _054_;
  wire _055_;
  wire _056_;
  wire _057_;
  wire _058_;
  wire _059_;
  wire _060_;
  wire _061_;
  output clk_1;
  reg clk_1;
  input clk_16;
  output clk_2;
  reg clk_2;
  output clk_4;
  reg clk_4;
  reg [2:0] contador_1;
  reg [1:0] contador_2;
  reg contador_4;
  input reset_L;
  NOT _062_ (
    .A(_031_),
    .Y(_012_)
  );
  NAND _063_ (
    .A(_037_),
    .B(_029_),
    .Y(_013_)
  );
  NOR _064_ (
    .A(_013_),
    .B(_012_),
    .Y(_015_)
  );
  NOR _065_ (
    .A(_015_),
    .B(_046_),
    .Y(_017_)
  );
  NAND _066_ (
    .A(_015_),
    .B(_046_),
    .Y(_018_)
  );
  NAND _067_ (
    .A(_018_),
    .B(_006_),
    .Y(_020_)
  );
  NOR _068_ (
    .A(_020_),
    .B(_017_),
    .Y(_007_)
  );
  NOT _069_ (
    .A(_006_),
    .Y(_022_)
  );
  NOR _070_ (
    .A(_037_),
    .B(_022_),
    .Y(_008_)
  );
  NOR _071_ (
    .A(_037_),
    .B(_029_),
    .Y(_024_)
  );
  NAND _072_ (
    .A(_013_),
    .B(_006_),
    .Y(_026_)
  );
  NOR _073_ (
    .A(_026_),
    .B(_024_),
    .Y(_009_)
  );
  NAND _074_ (
    .A(_013_),
    .B(_012_),
    .Y(_028_)
  );
  NAND _075_ (
    .A(_028_),
    .B(_006_),
    .Y(_030_)
  );
  NOR _076_ (
    .A(_030_),
    .B(_015_),
    .Y(_010_)
  );
  NOT _077_ (
    .A(_033_),
    .Y(_032_)
  );
  NOT _078_ (
    .A(_035_),
    .Y(_034_)
  );
  NOR _079_ (
    .A(_034_),
    .B(_032_),
    .Y(_036_)
  );
  NOR _080_ (
    .A(_036_),
    .B(_011_),
    .Y(_038_)
  );
  NAND _081_ (
    .A(_036_),
    .B(_011_),
    .Y(_039_)
  );
  NAND _082_ (
    .A(_039_),
    .B(_006_),
    .Y(_040_)
  );
  NOR _083_ (
    .A(_040_),
    .B(_038_),
    .Y(_014_)
  );
  NOR _084_ (
    .A(_033_),
    .B(_022_),
    .Y(_016_)
  );
  NAND _085_ (
    .A(_034_),
    .B(_032_),
    .Y(_041_)
  );
  NAND _086_ (
    .A(_041_),
    .B(_006_),
    .Y(_042_)
  );
  NOR _087_ (
    .A(_042_),
    .B(_036_),
    .Y(_019_)
  );
  NOR _088_ (
    .A(_023_),
    .B(_021_),
    .Y(_043_)
  );
  NAND _089_ (
    .A(_023_),
    .B(_021_),
    .Y(_044_)
  );
  NAND _090_ (
    .A(_044_),
    .B(_006_),
    .Y(_045_)
  );
  NOR _091_ (
    .A(_045_),
    .B(_043_),
    .Y(_025_)
  );
  NOR _092_ (
    .A(_023_),
    .B(_022_),
    .Y(_027_)
  );
  always @(posedge clk_16)
      clk_1 <= _000_;
  always @(posedge clk_16)
      contador_1[0] <= _003_[0];
  always @(posedge clk_16)
      contador_1[1] <= _003_[1];
  always @(posedge clk_16)
      contador_1[2] <= _003_[2];
  always @(posedge clk_16)
      clk_2 <= _001_;
  always @(posedge clk_16)
      contador_2[0] <= _004_[0];
  always @(posedge clk_16)
      contador_2[1] <= _004_[1];
  always @(posedge clk_16)
      clk_4 <= _002_;
  always @(posedge clk_16)
      contador_4 <= _005_;
  assign _046_ = clk_1;
  assign _006_ = reset_L;
  assign _000_ = _007_;
  assign _003_[0] = _008_;
  assign _003_[1] = _009_;
  assign _003_[2] = _010_;
  assign _011_ = clk_2;
  assign _001_ = _014_;
  assign _004_[0] = _016_;
  assign _004_[1] = _019_;
  assign _021_ = clk_4;
  assign _023_ = contador_4;
  assign _002_ = _025_;
  assign _005_ = _027_;
  assign _029_ = contador_1[1];
  assign _031_ = contador_1[2];
  assign _033_ = contador_2[0];
  assign _035_ = contador_2[1];
  assign _037_ = contador_1[0];
endmodule
