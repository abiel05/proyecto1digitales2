module greloj_p(
    input clk_4_estr,
    input clk_4_cond,
    input clk_2_estr,
    input clk_2_cond,
    input clk_1_estr,
    input clk_1_cond,
    output reg reset_L,
    output reg clk_16);

initial clk_16 <= 0;
always #2 clk_16 <= ~clk_16;

always @(posedge clk_16) begin
    reset_L <= 0;
    #16 reset_L <= 1;
    #100 $finish;
end

endmodule
