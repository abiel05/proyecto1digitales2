////////////////////////////////////////////////////////////////////////////////
//Definición módulo PCIe transmisor phy_tx 
////////////////////////////////////////////////////////////////////////////////
`include "greloj_cond.v"
`include "phy_tx_estructural_auto_mapped.v"
`include "cmos_cells.v"
`timescale 1ns/100ps
module BancoPruebas_phy_tx();

    wire [7:0] data_0, data_1, data_2, data_3;
    wire valid_data_0, valid_data_1, valid_data_2, valid_data_3;
    wire clk_f, clk_2f, clk_4f, clk_16f;
    wire reset_L, reset_clk;
    wire [1:0] data_out_cond, data_out_est;
    
    greloj_cond clks (/*autoinst*/
    .clk_4                          (clk_4f                                      ), // output
    .clk_2                          (clk_2f                                      ), // output
    .clk_1                          (clk_f                                      ), // output
    .reset_L                        (reset_clk                                     ), // input  // INST_NEW
    .clk_in                         (clk_16f                                     )  // input 

    // Salida de 4 ciclos reloj
    // 16 / 4 = 4 ciclos reloj
);

    probador_phy_tx ptx_0 (/*autoinst*/
    .data_out_cond                  (data_out_cond[1:0]                              ), // input 
    .data_out_est                   (data_out_est[1:0]                               ), // input 
    .clk_16f                        (clk_16f                                    ), // output
    .valid_data_0                   (valid_data_0                               ), // output
    .valid_data_1                   (valid_data_1                               ), // output
    .valid_data_2                   (valid_data_2                               ), // output
    .valid_data_3                   (valid_data_3                               ), // output
    .reset_L                        (reset_L                                    ), // output
    .reset_clk                      (reset_clk                                  ), // output // INST_NEW
    .data_0                         (data_0[7:0]                                ), // output
    .data_1                         (data_1[7:0]                                ), // output
    .data_2                         (data_2[7:0]                                ), // output
    .data_3                         (data_3[7:0]                                )  // output

        //Inicialización de registros de salida

        //Declaración de variables internas

        //Reloj base a 16f
);

    phy_tx phytx_cond_0 (/*autoinst*/
    .data_0                         (data_0[7:0]                                ), // input 
    .data_1                         (data_1[7:0]                                ), // input 
    .data_2                         (data_2[7:0]                                ), // input 
    .data_3                         (data_3[7:0]                                ), // input 
    .valid_data_0                   (valid_data_0                               ), // input 
    .valid_data_1                   (valid_data_1                               ), // input 
    .valid_data_2                   (valid_data_2                               ), // input 
    .valid_data_3                   (valid_data_3                               ), // input 
    .clk_f                          (clk_f                                      ), // input 
    .clk_2f                         (clk_2f                                     ), // input 
    .clk_4f                         (clk_4f                                     ), // input 
    .clk_16f                        (clk_16f                                    ), // input 
    .reset_L                        (reset_L                                    ), // input  // INST_NEW
    .data_out_p2s                   (data_out_cond[1:0]                          )  // output

    // by  2018-10-18 | // Declaración de variables internas 
    // by  2018-10-18 | Etapa flops @clk_f 
    // by  2018-10-17 | Mux L1 @ clk_2f 
    // by  2018-10-17 | Etapa flops @clk_2f 
    // by  2018-10-17 | Mux L2 @ clk_4f 

    // by  2018-10-17 | Etapa flops @clk_f 
);
    

    phy_tx_estructural_auto_mapped phytx_est_0 (/*autoinst*/
    .clk_16f                        (clk_16f                                    ), // input 
    .clk_2f                         (clk_2f                                     ), // input 
    .clk_4f                         (clk_4f                                     ), // input 
    .clk_f                          (clk_f                                      ), // input 
    .data_0                         (data_0[7:0]                                ), // input 
    .data_1                         (data_1[7:0]                                ), // input 
    .data_2                         (data_2[7:0]                                ), // input 
    .data_3                         (data_3[7:0]                                ), // input 
    .data_out_p2s                   (data_out_est[1:0]                          ), // output
    .reset_L                        (reset_L                                    ), // input  // INST_NEW
    .valid_data_0                   (valid_data_0                               ), // input 
    .valid_data_1                   (valid_data_1                               ), // input 
    .valid_data_2                   (valid_data_2                               ), // input 
    .valid_data_3                   (valid_data_3                               )  // input 
);
endmodule
