////////////////////////////////////////////////////////////////////////////////
//Definición módulo demux 2:1 con selector automático
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module demuxL2 (input clk,
                input reset_L,
                input [7:0] data_out,
                input valid_data_out,
                output reg [7:0] data_0,
                output reg valid_data_0,
                output reg [7:0] data_1,
                output reg valid_data_1);//Declaración de variables entrada-salida

    //Declaración de variables internas
    reg selector;

    //Lógica de salida data_0 y data_1
    always @(posedge clk)
    begin
        selector<=0;
        if(reset_L == 1)
        begin
            selector<=~selector;
            if(selector == 1)  //  si el selector esta en 1:
            begin
                if(valid_data_out == 1)  // si el dato es valido:
                begin
                    data_1[7:0] <= data_out[7:0];  //  pone el dato de out en data 1
                    valid_data_1 <= 1'b1;  //  pone valido el dato
                end
                else  //  si no es valido:
                begin
                    data_1[7:0] <= data_1[7:0];  //  mantiene el valor anterior de dato 1
                    valid_data_1 <= 1'b0;  //  pone no valido el dato
                end
            end
            else  //  si el selector esta en 0:
            begin
                if(valid_data_out == 1)  //  si el dato es valido:
                begin
                    data_0[7:0] <= data_out[7:0];  //  pone el dato de out en data 0
                    valid_data_0 <= 1'b1;  //  pone valido el dato
                end
                else  //  si no es valido:
                begin
                    data_0[7:0] <= data_0[7:0];  //  mantiene el valor anterior de dato 0
                    valid_data_0 <= 1'b0;  //  pone no valido el dato
                end
            end
        end
        else
        begin
            data_0[7:0]<=0;
            data_1[7:0]<=0;
            valid_data_0<=0;
            valid_data_1<=0;
        end
    end

endmodule
