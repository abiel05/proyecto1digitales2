////////////////////////////////////////////////////////////////////////////////
//Definición módulo de Banco de Pruebas
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
`include "demuxL1.v"
`include "demuxL1_synth.v"
`include "demuxL1_p.v"
module demuxL1_tb();

    //Declaración de variables internas
    wire clk, reset_L,valid_data_0_cond,valid_data_0_estr,valid_data_1_cond,valid_data_1_estr,valid_data_2_cond,valid_data_2_estr,valid_data_3_cond,valid_data_3_estr,valid_data_out_0,valid_data_out_1;
    wire [7:0] data_0_cond,data_0_estr,data_1_cond,data_1_estr,data_2_cond,data_2_estr,data_3_cond,data_3_estr,data_out_0,data_out_1;

    //Instanciando módulo Probador.
    demuxL1_p probador(.clk(clk),
            .reset_L(reset_L),
            .data_0_estr(data_0_estr),
            .valid_data_0_estr(valid_data_0_estr),
            .data_0_cond(data_0_cond),
            .valid_data_0_cond(valid_data_0_cond),
            .data_1_estr(data_1_estr),
            .valid_data_1_estr(valid_data_1_estr),
            .data_1_cond(data_1_cond),
            .valid_data_1_cond(valid_data_1_cond),
            .data_2_estr(data_2_estr),
            .valid_data_2_estr(valid_data_2_estr),
            .data_2_cond(data_2_cond),
            .valid_data_2_cond(valid_data_2_cond),
            .data_3_estr(data_3_estr),
            .valid_data_3_estr(valid_data_3_estr),
            .data_3_cond(data_3_cond),
            .valid_data_3_cond(valid_data_3_cond),
            .data_out_0(data_out_0),
            .valid_data_out_0(valid_data_out_0),
            .data_out_1(data_out_1),
            .valid_data_out_1(valid_data_out_1));

    //Instanciando módulo demux 4:2 con selector automático. Diseño conductual
    demuxL1 conductual(.clk(clk),
            .reset_L(reset_L),
            .data_0(data_0_cond),
            .valid_data_0(valid_data_0_cond),
            .data_1(data_1_cond),
            .valid_data_1(valid_data_1_cond),
            .data_2(data_2_cond),
            .valid_data_2(valid_data_2_cond),
            .data_3(data_3_cond),
            .valid_data_3(valid_data_3_cond),
            .data_out_0(data_out_0),
            .valid_data_out_0(valid_data_out_0),
            .data_out_1(data_out_1),
            .valid_data_out_1(valid_data_out_1));

    //Instanciando módulo demux 4:2 con selector automático. Diseño estructural Mapeado
    demuxL1_synth estructural(.clk(clk),
            .reset_L(reset_L),
            .data_0(data_0_estr),
            .valid_data_0(valid_data_0_estr),
            .data_1(data_1_estr),
            .valid_data_1(valid_data_1_estr),
            .data_2(data_2_estr),
            .valid_data_2(valid_data_2_estr),
            .data_3(data_3_estr),
            .valid_data_3(valid_data_3_estr),
            .data_out_0(data_out_0),
            .valid_data_out_0(valid_data_out_0),
            .data_out_1(data_out_1),
            .valid_data_out_1(valid_data_out_1));

endmodule
