////////////////////////////////////////////////////////////////////////////////
//Definición modulo de verificación
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module demuxL2_p (output reg clk,
                output reg reset_L,
                input [7:0] data_0_cond,
                input [7:0] data_0_estr,
                input valid_data_0_cond,
                input valid_data_0_estr,
                input valid_data_1_cond,
                input valid_data_1_estr,
                input [7:0] data_1_cond,
                input [7:0] data_1_estr,
                output reg valid_data_out,
                output reg [7:0] data_out);//Declaración de variables entrada-salida

    //Declaración de variables internas
//    reg compare;
    reg [7:0] conductual_data_0, conductual_data_1, estructural_data_0, estructural_data_1;

    //Inicialización de registros de salida
    initial
    begin
        reset_L=0;
        data_out[7:0]=0;
        valid_data_out=0;
        //compare=0;
        conductual_data_0[7:0]=8'b0;
        conductual_data_1[7:0]=8'b0;
        estructural_data_0[7:0]=8'b0;
        estructural_data_1[7:0]=8'b0;
    end

    //Reloj
    initial clk <= 0;
    always #1000 clk <= ~clk;

    //Asignación registros de salida
    always @(*)
    begin
        $dumpfile("senalesVCD_demuxL2.vcd");
        $dumpvars;
        $display ("Inicio Simulación");
        $monitor ($time,"\tclk = %b, reset_L = %b, data_0_cond = %h, data_0_estr = %h, data_1_cond = %h, data_1_estr = %h, data_out = %h", clk, reset_L, data_0_cond, data_0_estr, data_1_cond, data_1_estr, data_out);
        #5000 reset_L = 1;
        data_out[7:0] = 8'h8;
        //#4000 data_out[7:0] = 8'h8;
        #2000 valid_data_out = 1;
        #2000 valid_data_out = 0;
        #2000 valid_data_out = 1;
        data_out[7:0] = 8'hF;
        #2000 valid_data_out = 1;
//        #2000 valid_data_out = 0;
        #1000 valid_data_out = 1;
        #5000 data_out[7:0] = 8'hE;
        valid_data_out = 0;
        #5000 data_out[7:0] = 8'hE;
        valid_data_out = 1;
        #20000 reset_L = 0;
        #5000 data_out[7:0] = 8'hA;
        #5000 reset_L = 1;
        #10000 data_out[7:0] = 8'h6;
        #2000 valid_data_out = 1;
        #2000 valid_data_out = 0;
        #2000 valid_data_out = 1;
        #2000 data_out[7:0] = 8'hC;
        #2000 valid_data_out = 0;
        #2000 valid_data_out = 1;
        #10000 $finish;
    end

    //Asignación de salidas de los demux a registros internos de probador.v para comparación
    always @(posedge clk)
    begin
        conductual_data_0[7:0] <= data_0_cond[7:0];
        conductual_data_1[7:0] <= data_1_cond[7:0];
        estructural_data_0[7:0] <= data_0_estr[7:0];
        estructural_data_1[7:0] <= data_1_estr[7:0];
    end

    //Lógica de comparación
//    always @(*)
//    begin
//        compare = 0;
//        if(conductual != estructural)
//        begin
//            compare = 1;
//        end
//    end

endmodule
