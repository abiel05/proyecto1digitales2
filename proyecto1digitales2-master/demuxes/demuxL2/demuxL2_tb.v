////////////////////////////////////////////////////////////////////////////////
//Definición módulo de Banco de Pruebas
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
`include "demuxL2.v"
`include "demuxL2_synth.v"
`include "demuxL2_p.v"
module demuxL2_tb();

    //Declaración de variables internas
    wire clk, reset_L,valid_data_0_cond,valid_data_0_estr,valid_data_1_cond,valid_data_1_estr,valid_data_out;
    wire [7:0] data_0_cond,data_0_estr,data_1_cond,data_1_estr,data_out;

    //Instanciando módulo Probador.
    demuxL2_p probador(.clk(clk),
            .reset_L(reset_L),
            .data_0_estr(data_0_estr),
            .valid_data_0_estr(valid_data_0_estr),
            .data_0_cond(data_0_cond),
            .valid_data_0_cond(valid_data_0_cond),
            .data_1_estr(data_1_estr),
            .valid_data_1_estr(valid_data_1_estr),
            .data_1_cond(data_1_cond),
            .valid_data_1_cond(valid_data_1_cond),
            .data_out(data_out),
            .valid_data_out(valid_data_out));

    //Instanciando módulo demux 4:2 con selector automático. Diseño conductual
    demuxL2 conductual(.clk(clk),
            .reset_L(reset_L),
            .data_0(data_0_cond),
            .valid_data_0(valid_data_0_cond),
            .data_1(data_1_cond),
            .valid_data_1(valid_data_1_cond),
            .data_out(data_out),
            .valid_data_out(valid_data_out));

    //Instanciando módulo demux 4:2 con selector automático. Diseño estructural Mapeado
    demuxL2 estructural(.clk(clk),
            .reset_L(reset_L),
            .data_0(data_0_estr),
            .valid_data_0(valid_data_0_estr),
            .data_1(data_1_estr),
            .valid_data_1(valid_data_1_estr),
            .data_out(data_out),
            .valid_data_out(valid_data_out));

endmodule
