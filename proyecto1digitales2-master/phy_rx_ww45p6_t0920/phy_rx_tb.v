`include "phy_rx.v"
`include "phy_rx_p.v"
`include "greloj_cond.v"
// by  2018-10-21 | `include "phy_rx_estructural_auto_mapped.v"
`include "cmos_cells.v"
`timescale 1ns/100ps
module phy_rx_tb();

    wire [7:0] data_out_0_cond, data_out_1_cond, data_out_2_cond, data_out_3_cond;
    wire valid_data_out_0_cond, valid_data_out_1_cond, valid_data_out_2_cond, valid_data_out_3_cond;
// by  2018-10-21 |     wire [7:0] data_out_0_estr, data_out_1_estr, data_out_2_estr, data_out_3_estr;
// by  2018-10-21 |     wire valid_data_out_0_estr, valid_data_out_1_estr, valid_data_out_2_estr, valid_data_out_3_estr;
    wire [1:0] data_in_probador;
    wire clk_1, clk_2, clk_4, clk_16;
    wire reset_L;

    reg [7:0] data_out_0_estr, data_out_1_estr, data_out_2_estr, data_out_3_estr;
    reg valid_data_out_0_estr, valid_data_out_1_estr, valid_data_out_2_estr, valid_data_out_3_estr;

    always
    begin
        #1 data_out_0_estr[7:0] = 8'b0;
        #1 data_out_1_estr[7:0] = 8'b0;
        #1 data_out_2_estr[7:0] = 8'b0;
        #1 data_out_3_estr[7:0] = 8'b0;
        #1 valid_data_out_0_estr = 0;
        #1 valid_data_out_1_estr = 0;
        #1 valid_data_out_2_estr = 0;
        #1 valid_data_out_3_estr = 0;
    end


    greloj_cond clks (/*AUTOINST*/
        // Outputs
        .clk_4		(clk_4),
        .clk_2		(clk_2),
        .clk_1		(clk_1),
        // Inputs
        .reset_L		(reset_L),
        .clk_in		(clk_16));

    phy_rx_p prx_0 (/*AUTOINST*/
        // Outputs
        .clk_16		(clk_16),
        .reset_L		(reset_L),
        .data_in_probador	(data_in_probador[1:0]),
        // Inputs
        .data_out_0_cond	(data_out_0_cond[7:0]),
        .data_out_0_estr	(data_out_0_estr[7:0]),
        .data_out_1_cond	(data_out_1_cond[7:0]),
        .data_out_1_estr	(data_out_1_estr[7:0]),
        .data_out_2_cond	(data_out_2_cond[7:0]),
        .data_out_2_estr	(data_out_2_estr[7:0]),
        .data_out_3_cond	(data_out_3_cond[7:0]),
        .data_out_3_estr	(data_out_3_estr[7:0]),
        .valid_data_out_0_cond(valid_data_out_0_cond),
        .valid_data_out_0_estr(valid_data_out_0_estr),
        .valid_data_out_1_cond(valid_data_out_1_cond),
        .valid_data_out_1_estr(valid_data_out_1_estr),
        .valid_data_out_2_cond(valid_data_out_2_cond),
        .valid_data_out_2_estr(valid_data_out_2_estr),
        .valid_data_out_3_cond(valid_data_out_3_cond),
        .valid_data_out_3_estr(valid_data_out_3_estr));

    phy_rx phyrx_cond_0 (/*AUTOINST*/
        // Outputs
        .data_out_0		(data_out_0_cond[7:0]),
        .data_out_1		(data_out_1_cond[7:0]),
        .data_out_2		(data_out_2_cond[7:0]),
        .data_out_3		(data_out_3_cond[7:0]),
        .valid_data_out_0	(valid_data_out_0_cond),
        .valid_data_out_1	(valid_data_out_1_cond),
        .valid_data_out_2	(valid_data_out_2_cond),
        .valid_data_out_3	(valid_data_out_3_cond),
        // Inputs
        .clk_1		    (clk_1),
        .clk_2	    	(clk_2),
        .clk_4    		(clk_4),
        .clk_16		(clk_16),
        .reset_L		(reset_L),
        .data_in       (data_in_probador[1:0])); // input  // INST_NEW
    
// by  2018-10-21 |     phy_rx_estructural_auto_mapped phyrx_est_0 (/*AUTOINST*/
// by  2018-10-21 |         .clk_1                          (clk_1                                      ), // input 
// by  2018-10-21 |         .clk_16                         (clk_16                                     ), // input 
// by  2018-10-21 |         .clk_2                          (clk_2                                      ), // input 
// by  2018-10-21 |         .clk_4                          (clk_4                                      ), // input 
// by  2018-10-21 |         .data_in                        (data_in[1:0]                               ), // input 
// by  2018-10-21 |         .data_out_0                     (data_out_0_estr[7:0]                            ), // output
// by  2018-10-21 |         .data_out_1                     (data_out_1_estr[7:0]                            ), // output
// by  2018-10-21 |         .data_out_2                     (data_out_2_estr[7:0]                            ), // output
// by  2018-10-21 |         .data_out_3                     (data_out_3_estr[7:0]                            ), // output
// by  2018-10-21 |         .reset_L                        (reset_L                                    ), // input 
// by  2018-10-21 |         .valid_data_out_0               (valid_data_out_0_estr                           ), // output
// by  2018-10-21 |         .valid_data_out_1               (valid_data_out_1_estr                           ), // output
// by  2018-10-21 |         .valid_data_out_2               (valid_data_out_2_estr                           ), // output
// by  2018-10-21 |         .valid_data_out_3               (valid_data_out_3_estr                           )  // output
// by  2018-10-21 |         );

endmodule
