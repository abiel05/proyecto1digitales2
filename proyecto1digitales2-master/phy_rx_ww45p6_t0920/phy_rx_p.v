module phy_rx_p (
    output reg clk_16,
    output reg reset_L,
    output reg [1:0] data_in_probador,
    input [7:0] data_out_0_cond,
    input [7:0] data_out_0_estr,
    input [7:0] data_out_1_cond,
    input [7:0] data_out_1_estr,
    input [7:0] data_out_2_cond,
    input [7:0] data_out_2_estr,
    input [7:0] data_out_3_cond,
    input [7:0] data_out_3_estr,
    input valid_data_out_0_cond,
    input valid_data_out_0_estr,
    input valid_data_out_1_cond,
    input valid_data_out_1_estr,
    input valid_data_out_2_cond,
    input valid_data_out_2_estr,
    input valid_data_out_3_cond,
    input valid_data_out_3_estr);

   //Declaración de variables internas
    reg compare;
    reg [31:0] conductual, estructural;
   
    //Inicialización de registros de salida
    initial
    begin
        clk_16=0;
        reset_L=0;
        data_in_probador[1:0]=0;
        compare=0;
        conductual[31:0]=32'b0;
        estructural[31:0]=32'b0;
    end

    //Reloj base a 16f
    always
    begin
	#1 clk_16 = ~clk_16;
    end

    //Asignación registros de salida
    initial
//    always @(*)
    begin
        $dumpfile("s2p.vcd");
        $dumpvars;
        $display ("Inicio Simulación!!");
//        $monitor ($time,"\tclk = %b, data_in = %h, data_out_cond = %h, data_out_est = %h", clk, data_in, data_out, data_out);
	#5 reset_L = 1'b1;
        #5 data_in_probador[1:0] = 2'b11;
        #5 data_in_probador[1:0] = 2'b00;
        #5 data_in_probador[1:0] = 2'b10;
        #5 data_in_probador[1:0] = 2'b01;
        #5 data_in_probador[1:0] = 2'b01;
        #5 data_in_probador[1:0] = 2'b10;
        #5 data_in_probador[1:0] = 2'b11;
        #5 data_in_probador[1:0] = 2'b01;
        #5 data_in_probador[1:0] = 2'b00;

        #25 data_in_probador[1:0] = 2'b00;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b10;
        #2 data_in_probador[1:0] = 2'b00;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b10;
        #2 data_in_probador[1:0] = 2'b00;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b10;
        #2 data_in_probador[1:0] = 2'b00;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b10;
        #2 data_in_probador[1:0] = 2'b00;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b10;
        #2 data_in_probador[1:0] = 2'b00;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b10;
        #2 data_in_probador[1:0] = 2'b00;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b10;
        #2 data_in_probador[1:0] = 2'b00;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b00;
        #2 data_in_probador[1:0] = 2'b10;
        #2 data_in_probador[1:0] = 2'b01;
        #2 data_in_probador[1:0] = 2'b01;
        #2 data_in_probador[1:0] = 2'b10;
        #2 data_in_probador[1:0] = 2'b11;
        #2 data_in_probador[1:0] = 2'b01;
        #2 data_in_probador[1:0] = 2'b00;
	#20 $finish;
    end


    //Asignación de salidas de los multiplexores a registros internos de probador.v para comparación
    always @(posedge clk_16)
    begin
        conductual[31:0] <= {data_out_0_cond[7:0],data_out_1_cond[7:0],data_out_2_cond[7:0],data_out_3_cond[7:0]};
        estructural[31:0] <= {data_out_0_estr[7:0],data_out_1_estr[7:0],data_out_2_estr[7:0],data_out_3_estr[7:0]};
    end
    //Lógica de comparación
    always @(*)
    begin
        compare = 0;
        if(conductual != estructural)
        begin
            compare = 1;
        end
    end

endmodule
