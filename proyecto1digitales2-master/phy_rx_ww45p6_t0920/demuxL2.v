////////////////////////////////////////////////////////////////////////////////
//Definición módulo demux 2:1 con selector automático
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module demuxL2 (
    input clk,
    input reset_L,
    output reg [7:0] data_out_0,
    output reg valid_data_out_0,
    output reg valid_data_out_1,
    output reg [7:0] data_out_1,
    input valid_data_in,
    input [7:0] data_in);//Declaración de variables entrada-salida

    //Declaración de variables internas
    reg selector;

    //Lógica de salida data_out_0 y data_out_1
    always @(posedge clk)
    begin
        selector<=0;
        if(reset_L == 1)
        begin
            selector<=~selector;
            if(selector == 1)  //  si el selector esta en 1:
            begin
                if(valid_data_in == 1)  // si el dato es valido:
                begin
                    data_out_1[7:0] <= data_in[7:0];  //  pone el dato de out en data 1
                    valid_data_out_1 <= 1'b1;  //  pone valido el dato
                end
                else  //  si no es valido:
                begin
                    data_out_1[7:0] <= data_out_1[7:0];  //  mantiene el valor anterior de dato 1
                    valid_data_out_1 <= 1'b0;  //  pone no valido el dato
                end
            end
            else  //  si el selector esta en 0:
            begin
                if(valid_data_in == 1)  //  si el dato es valido:
                begin
                    data_out_0[7:0] <= data_in[7:0];  //  pone el dato de out en data 0
                    valid_data_out_0 <= 1'b1;  //  pone valido el dato
                end
                else  //  si no es valido:
                begin
                    data_out_0[7:0] <= data_out_0[7:0];  //  mantiene el valor anterior de dato 0
                    valid_data_out_0 <= 1'b0;  //  pone no valido el dato
                end
            end
        end
        else
        begin
            data_out_0[7:0]<=0;
            data_out_1[7:0]<=0;
            valid_data_out_0<=0;
            valid_data_out_1<=0;
        end
    end

endmodule
