module reloj(CLK);
  output CLK;
  reg CLK;

  initial 
    begin
      CLK = 0;
    end
	
  always
    begin
      #1000 CLK = ~CLK;
    end
	
endmodule
