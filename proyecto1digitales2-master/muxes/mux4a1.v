////////////////////////////////////////////////////////////////////////////////
//Definición módulo Multiplexor 2:1 con selector automático
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
`include "mux.v"
module mux4a1 (input clk,
            input reset_L,
            input [3:0] data_0,
            input valid_data_0,
            input [3:0] data_1,
            input valid_data_1,
            input [3:0] data_2,
            input valid_data_2,
            input [3:0] data_3,
            input valid_data_3,
            output valid_data_out,
            output [3:0] data_out);//Declaración de variables entrada-salida

    //Declaración de variables internas
    wire [3:0] data_mux_0, data_mux_1;
    wire valid_data_mux_0, valid_data_mux_1;

    //Instanciando 3 módulos Multiplexor 2:1 con selector automático para formar 1 Multiplexor 4:1 con selector automático.
    mux mux_0(.clk(clk),  
                   .reset_L(reset_L),   
                   .data_0(data_0),   
                   .data_1(data_1),   
                   .data_out(data_mux_0),   
                   .valid_data_0(valid_data_0),   
                   .valid_data_1(valid_data_1),   
                   .valid_data_out(valid_data_mux_0));

    mux mux_1(.clk(clk),  
                   .reset_L(reset_L),   
                   .data_0(data_2),   
                   .data_1(data_3),   
                   .data_out(data_mux_1),   
                   .valid_data_0(valid_data_2),   
                   .valid_data_1(valid_data_3),   
                   .valid_data_out(valid_data_mux_1));

    mux mux_Out(.clk(clk),  
                   .reset_L(reset_L),   
                   .data_0(data_mux_0),   
                   .data_1(data_mux_1),   
                   .data_out(data_out),   
                   .valid_data_0(valid_data_mux_0),   
                   .valid_data_1(valid_data_mux_1),   
                   .valid_data_out(valid_data_out));

endmodule
