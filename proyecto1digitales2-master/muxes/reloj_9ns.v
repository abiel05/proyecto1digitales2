module reloj(CLK);
  output CLK;
  reg CLK;

  initial 
    begin
      CLK = 0;
    end
	
  always
    begin
      #9 CLK = ~CLK;
    end
	
endmodule
