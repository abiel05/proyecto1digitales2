////////////////////////////////////////////////////////////////////////////////
//Definición módulo de Banco de Pruebas
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
`include "muxL1_estructural_auto_mapped.v"
`include "cmos_cells_delays.v"
`include "TimeChecks.v"
module BancoPruebas();

    //Declaración de variables internas  
    wire clk, reset_L,valid_data_0,valid_data_1,valid_data_2,valid_data_3,valid_data_out_cond,valid_data_out_est;
    wire [7:0] data_0, data_1, data_2, data_3, data_out_cond, data_out_est;

    //Instanciando módulos Reloj y Probador.
    reloj r0(.CLK(clk));
    probador t1(.clk(clk),
            .reset_L(reset_L),
            .data_0(data_0),
            .valid_data_0(valid_data_0),
            .data_1(data_1),
            .valid_data_1(valid_data_1),
            .data_2(data_2),
            .valid_data_2(valid_data_2),
            .data_3(data_3),
            .valid_data_3(valid_data_3),
            .data_out(data_out_cond),
            .valid_data_out(valid_data_out_cond),
            .data_out_estructural(data_out_est),
            .valid_data_out_est(valid_data_out_est));  

    //Instanciando módulo Multiplexor 4:1 con selector automático. Diseño conductual
    muxL1 muxL1_cond_0 (.clk(clk),
            .reset_L(reset_L),
            .data_0(data_0),
            .valid_data_0(valid_data_0),
            .data_1(data_1),
            .valid_data_1(valid_data_1),
            .data_2(data_2),
            .valid_data_2(valid_data_2),
            .data_3(data_3),
            .valid_data_3(valid_data_3),
            .data_out(data_out_cond),
            .valid_data_out(valid_data_out_cond));

    //Instanciando módulo Multiplexor 4:1 con selector automático. Diseño estructural Mapeado
    muxL1_estructural_mapped muxL1_est_0 (.clk(clk),
            .reset_L(reset_L),
            .data_0(data_0),
            .valid_data_0(valid_data_0),
            .data_1(data_1),
            .valid_data_1(valid_data_1),
            .data_2(data_2),
            .valid_data_2(valid_data_2),
            .data_3(data_3),
            .valid_data_3(valid_data_3),
            .data_out(data_out_est),
            .valid_data_out(valid_data_out_est));

endmodule 
