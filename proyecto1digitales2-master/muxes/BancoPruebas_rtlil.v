////////////////////////////////////////////////////////////////////////////////
//Definición módulo de Banco de Pruebas
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
`include "mux_estructural_auto_rtlil.v"
module BancoPruebas();

    //Declaración de variables internas
    wire clk, reset_L,valid_data_0,valid_data_1,valid_data_2,valid_data_3,valid_data_out_cond_0,valid_data_out_est_0,valid_data_out_cond_1,valid_data_out_est_1;
    wire [7:0] data_0, data_1, data_2, data_3, data_out_cond_0, data_out_cond_1, data_out_est_0, data_out_est_1;

    //Instanciando módulos Reloj y Probador.
    reloj r0(.CLK(clk));
    probador t1(.clk(clk),
            .reset_L(reset_L),
            .data_0(data_0),
            .valid_data_0(valid_data_0),
            .data_1(data_1),
            .valid_data_1(valid_data_1),
            .data_2(data_2),
            .valid_data_2(valid_data_2),
            .data_3(data_3),
            .valid_data_3(valid_data_3),
            .data_out_cond_0(data_out_cond_0),
            .valid_data_out_cond_0(valid_data_out_cond_0),
            .data_out_cond_1(data_out_cond_1),
            .valid_data_out_cond_1(valid_data_out_cond_1),
            .data_out_est_0(data_out_est_0),
            .valid_data_out_est_0(valid_data_out_est_0),
            .data_out_est_1(data_out_est_1),
            .valid_data_out_est_1(valid_data_out_est_1));

    //Instanciando módulo Multiplexor 4:2 con selector automático. Diseño conductual
    muxL1 muxL1_cond_0 (.clk(clk),
            .reset_L(reset_L),
            .data_0(data_0),
            .valid_data_0(valid_data_0),
            .data_1(data_1),
            .valid_data_1(valid_data_1),
            .data_2(data_2),
            .valid_data_2(valid_data_2),
            .data_3(data_3),
            .valid_data_3(valid_data_3),
            .data_out_0(data_out_cond_0),
            .valid_data_out_0(valid_data_out_cond_0),
            .data_out_1(data_out_cond_1),
            .valid_data_out_1(valid_data_out_cond_1));

    //Instanciando módulo Multiplexor 4:2 con selector automático. Diseño estructural Mapeado
    muxL1_estructural_rtlil muxL1_est_0 (.clk(clk),
            .reset_L(reset_L),
            .data_0(data_0),
            .valid_data_0(valid_data_0),
            .data_1(data_1),
            .valid_data_1(valid_data_1),
            .data_2(data_2),
            .valid_data_2(valid_data_2),
            .data_3(data_3),
            .valid_data_3(valid_data_3),
            .data_out_0(data_out_est_0),
            .valid_data_out_0(valid_data_out_est_0),
            .data_out_1(data_out_est_1),
            .valid_data_out_1(valid_data_out_est_1));

endmodule
