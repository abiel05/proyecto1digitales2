////////////////////////////////////////////////////////////////////////////////
//Definición módulo Multiplexor 2:1 con selector automático
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module muxL2 (input clk,
            input reset_L,
            input [7:0] data_0,
            input valid_data_0,
            input valid_data_1,
            input [7:0] data_1,
            output reg valid_data_out,
            output reg [7:0] data_out);//Declaración de variables entrada-salida

    //Declaración de variables internas
// by  2018-10-28 |     reg selector;
    wire valid_out, valid_both, valid_one, valid_select;
    wire [7:0] data_entry_select, data_valid_select, data_entry_valid, data_valid_out;
    wire selector;

    //changes
    wire sreset_0, sreset_1, sreset_2;
    reg sreset_out, mux_selector;


    assign sreset_0 = reset_L & (~sreset_out);// & (~clk);  
    assign sreset_1 = (~reset_L) & sreset_out;// & (~clk);   
    assign sreset_2 = sreset_0 | sreset_1; 


    always @(posedge clk)
    begin
        mux_selector <= sreset_2;
    end
    always @(negedge clk)
    begin
        sreset_out <= selector;
    end

    assign selector = (reset_L)?mux_selector:1'b0; 

    // Logica de salida
    always @(posedge clk)
    begin
        if(reset_L)
        begin
            data_out[7:0]<=data_valid_out[7:0];
// by  2018-10-28 |             selector<=selector+1;
            valid_data_out<=valid_out;
        end
        else
        begin
            data_out[7:0]<=0;
// by  2018-10-28 |             selector<=0;
            valid_data_out<=0;
        end
    end

    assign valid_out = valid_data_0 | valid_data_1;
    assign valid_both = valid_data_0 & valid_data_1;
    assign valid_one = valid_data_0 ^ valid_data_1;
    assign valid_select = valid_one & valid_data_1;
    assign data_valid_out[7:0] = (valid_out)?data_entry_valid[7:0]:data_out[7:0];
    assign data_entry_valid[7:0] = (valid_both)?data_entry_select[7:0]: data_valid_select[7:0];
    assign data_valid_select[7:0] = (valid_select)?data_1[7:0]: data_0[7:0];
    assign data_entry_select[7:0] = (selector)?data_1[7:0]:data_0[7:0];

endmodule
