`include "greloj_cond.v"
`include "greloj_p.v"
`include "cmos_cells.v"

`include "greloj_estructural_auto_mapped.v"

module greloj_tb;

wire wclk_4_cond;
wire wclk_4_estr;
wire wclk_2_cond;
wire wclk_2_estr;
wire wclk_1_cond;
wire wclk_1_estr;
wire wreset_L;
wire wclk_16;

greloj_cond gr_cond(.clk_4(wclk_4_cond),.clk_2(wclk_2_cond),.clk_1(wclk_1_cond),.reset_L(wreset_L),.clk_16(wclk_16));
greloj_estructural_mapped gr_estr(.clk_4(wclk_4_estr),.clk_2(wclk_2_estr),.clk_1(wclk_1_estr),.reset_L(wreset_L),.clk_16(wclk_16));
greloj_p gr_p(.clk_4_estr(wclk_4_estr),.clk_4_cond(wclk_4_cond),.clk_2_estr(wclk_2_estr),.clk_2_cond(wclk_2_cond),.clk_1_estr(wclk_1_estr),.clk_1_cond(wclk_1_cond),.reset_L(wreset_L),.clk_16(wclk_16));

    initial begin
        $dumpfile("senalesVCD_mapped_greloj.vcd");
        $dumpvars;
    end

endmodule
