////////////////////////////////////////////////////////////////////////////////
//Definición módulo banco de preubas del PCIe phy
////////////////////////////////////////////////////////////////////////////////

`include "cmos_cells.v"
`include "phy_estructural_auto_mapped.v"

`timescale 1ns/100ps
module BancoPruebas_phy();

    wire clk_16;
    wire reset_L, reset_clk;
    wire [7:0] data_in_0, data_in_1, data_in_2, data_in_3;
    wire valid_data_in_0, valid_data_in_1, valid_data_in_2, valid_data_in_3;
    wire [7:0] data_out_0_cond, data_out_1_cond, data_out_2_cond, data_out_3_cond;
    wire valid_data_out_0_cond, valid_data_out_1_cond, valid_data_out_2_cond, valid_data_out_3_cond;
    wire [7:0] data_out_0_estr, data_out_1_estr, data_out_2_estr, data_out_3_estr;
    wire valid_data_out_0_estr, valid_data_out_1_estr, valid_data_out_2_estr, valid_data_out_3_estr;

    //modulo phy probador
    probador_phy p_0 (/*AUTOINST*/
		      // Outputs
		      .clk_16		(clk_16),
		      .reset_L		(reset_L),
		      .reset_clk	(reset_clk),
		      .data_in_0	(data_in_0[7:0]),
		      .data_in_1	(data_in_1[7:0]),
		      .data_in_2	(data_in_2[7:0]),
		      .data_in_3	(data_in_3[7:0]),
		      .valid_data_in_0	(valid_data_in_0),
		      .valid_data_in_1	(valid_data_in_1),
		      .valid_data_in_2	(valid_data_in_2),
		      .valid_data_in_3	(valid_data_in_3),
		      // Inputs
		      .data_out_0_cond	(data_out_0_cond[7:0]),
		      .data_out_0_estr	(data_out_0_estr[7:0]),
		      .data_out_1_cond	(data_out_1_cond[7:0]),
		      .data_out_1_estr	(data_out_1_estr[7:0]),
		      .data_out_2_cond	(data_out_2_cond[7:0]),
		      .data_out_2_estr	(data_out_2_estr[7:0]),
		      .data_out_3_cond	(data_out_3_cond[7:0]),
		      .data_out_3_estr	(data_out_3_estr[7:0]),
		      .valid_data_out_0_cond(valid_data_out_0_cond),
		      .valid_data_out_0_estr(valid_data_out_0_estr),
		      .valid_data_out_1_cond(valid_data_out_1_cond),
		      .valid_data_out_1_estr(valid_data_out_1_estr),
		      .valid_data_out_2_cond(valid_data_out_2_cond),
		      .valid_data_out_2_estr(valid_data_out_2_estr),
		      .valid_data_out_3_cond(valid_data_out_3_cond),
		      .valid_data_out_3_estr(valid_data_out_3_estr));

    //modulo conductual
    phy phy_cond_0 (/*AUTOINST*/
		    // Outputs
		    .data_out_0		(data_out_0_cond[7:0]),
		    .data_out_1		(data_out_1_cond[7:0]),
		    .data_out_2		(data_out_2_cond[7:0]),
		    .data_out_3		(data_out_3_cond[7:0]),
		    .valid_data_out_0	(valid_data_out_0_cond),
		    .valid_data_out_1	(valid_data_out_1_cond),
		    .valid_data_out_2	(valid_data_out_2_cond),
		    .valid_data_out_3	(valid_data_out_3_cond),
		    // Inputs
		    .clk_16		(clk_16),
		    .reset_L		(reset_L),
            .reset_clk      (reset_clk),
		    .data_in_0		(data_in_0[7:0]),
		    .data_in_1		(data_in_1[7:0]),
		    .data_in_2		(data_in_2[7:0]),
		    .data_in_3		(data_in_3[7:0]),
		    .valid_data_in_0	(valid_data_in_0),
		    .valid_data_in_1	(valid_data_in_1),
		    .valid_data_in_2	(valid_data_in_2),
		    .valid_data_in_3	(valid_data_in_3));

    //modulo estructural
    phy_estructural_auto_mapped phy_estr_0 (/*AUTOINST*/
					     // Outputs
					     .data_out_0	(data_out_0_estr[7:0]),
					     .data_out_1	(data_out_1_estr[7:0]),
					     .data_out_2	(data_out_2_estr[7:0]),
					     .data_out_3	(data_out_3_estr[7:0]),
					     .valid_data_out_0	(valid_data_out_0_estr),
					     .valid_data_out_1	(valid_data_out_1_estr),
					     .valid_data_out_2	(valid_data_out_2_estr),
					     .valid_data_out_3	(valid_data_out_3_estr),
					     // Inputs
					     .clk_16		(clk_16),
					     .data_in_0		(data_in_0[7:0]),
					     .data_in_1		(data_in_1[7:0]),
					     .data_in_2		(data_in_2[7:0]),
					     .data_in_3		(data_in_3[7:0]),
					     .reset_L		(reset_L),
                         .reset_clk     (reset_clk),
					     .valid_data_in_0	(valid_data_in_0),
					     .valid_data_in_1	(valid_data_in_1),
					     .valid_data_in_2	(valid_data_in_2),
					     .valid_data_in_3	(valid_data_in_3));

endmodule
