////////////////////////////////////////////////////////////////////////////////
//Definición módulo PCIe phy
////////////////////////////////////////////////////////////////////////////////
`include "greloj_cond.v"
`include "phy_tx.v"
`include "phy_rx.v"

`timescale 1ns/100ps

module phy (
	input clk_16,
	input reset_L,
	input reset_clk,
	input [7:0] data_in_0,
	input [7:0] data_in_1,
	input [7:0] data_in_2,
	input [7:0] data_in_3,
	input valid_data_in_0,
	input valid_data_in_1,
	input valid_data_in_2,
	input valid_data_in_3,
	output reg [7:0] data_out_0,
	output reg [7:0] data_out_1,
	output reg [7:0] data_out_2,
	output reg [7:0] data_out_3,
	output reg valid_data_out_0,
	output reg valid_data_out_1,
	output reg valid_data_out_2,
	output reg valid_data_out_3);

	
	wire clk_4, clk_2, clk_1;		//cable para clks salida del modulo generador de relojes
	wire [1:0] data_out_p2s;		//cable para salida del p2s
	reg [1:0] data_in_s2p_0, data_in_s2p_1; //, data_in_s2p_2, data_in_s2p_3, data_in_s2p_4  ;		//flop para el paso del p2s al s2p
    wire [7:0] data_out_dmL1_0, data_out_dmL1_1, data_out_dmL1_2, data_out_dmL1_3;		//cable para paso de dato final al flop de salida
    wire valid_data_out_dmL1_0, valid_data_out_dmL1_1, valid_data_out_dmL1_2, valid_data_out_dmL1_3;		//cable para paso de valid final al flop de salida

    //modulo greloj_cond
    greloj_cond gr_0 (/*AUTOINST*/
		      // Outputs
		      .clk_4		(clk_4),	//va a cable clk_4
		      .clk_2		(clk_2),	//va a cable clk_2
		      .clk_1		(clk_1),	//va a cable clk_1
		      // Inputs
		      .reset_L		(reset_clk),		//entra el reset_L input del modulo phy
		      .clk_in		(clk_16));		//entra el clk input del modulo phy

	//modulo phy_tx
	phy_tx ptx_0 (/*AUTOINST*/
		      // Outputs
		      .data_out_p2s	(data_out_p2s[1:0]),
		      // Inputs
		      .data_0		(data_in_0[7:0]),		//entra el dato 1 entrada
		      .data_1		(data_in_1[7:0]),		//entra el dato 2 entrada
		      .data_2		(data_in_2[7:0]),		//entra el dato 3 entrada
		      .data_3		(data_in_3[7:0]),		//entra el dato 4 entrada
		      .valid_data_0	(valid_data_in_0),		//entra el valid del dato 1 entrada
		      .valid_data_1	(valid_data_in_1),		//entra el valid del dato 2 entrada
		      .valid_data_2	(valid_data_in_2),		//entra el valid del dato 3 entrada
		      .valid_data_3	(valid_data_in_3),		//entra el valid del dato 4 entrada
		      .clk_f		(clk_1),		//entra el cable del clk_1 salida del generador
		      .clk_2f		(clk_2),		//entra el cable del clk_1 salida del generador
		      .clk_4f		(clk_4),		//entra el cable del clk_1 salida del generador
		      .clk_16f		(clk_16),		//entra el clk input del modulo phy
		      .reset_L		(reset_L));		//entra el reset_L input del modulo phy

	always @ (posedge clk_16)
	begin
		data_in_s2p_0[1:0]<=data_out_p2s[1:0];		//se pasa el dato serial del p2s a un flop para pasarse luego al s2p (quitar si no es necesario)
		data_in_s2p_1[1:0]<=data_in_s2p_0[1:0];		//se pasa el dato serial del p2s a un flop para pasarse luego al s2p (quitar si no es necesario)
// by  2018-10-24 | 		data_in_s2p_2[1:0]<=data_in_s2p_1[1:0];		//se pasa el dato serial del p2s a un flop para pasarse luego al s2p (quitar si no es necesario)
// by  2018-10-24 | 		data_in_s2p_3[1:0]<=data_in_s2p_2[1:0];		//se pasa el dato serial del p2s a un flop para pasarse luego al s2p (quitar si no es necesario)
// by  2018-10-24 | 		data_in_s2p_4[1:0]<=data_in_s2p_3[1:0];		//se pasa el dato serial del p2s a un flop para pasarse luego al s2p (quitar si no es necesario)
	end


	//modulo phy_rx
	phy_rx prx_0 (/*AUTOINST*/
		      // Outputs
		      .data_out_0	(data_out_dmL1_0[7:0]),		//salida dato 1 final
		      .data_out_1	(data_out_dmL1_1[7:0]),		//salida dato 2 final
		      .data_out_2	(data_out_dmL1_2[7:0]),		//salida dato 3 final
		      .data_out_3	(data_out_dmL1_3[7:0]),		//salida dato 4 final
		      .valid_data_out_0	(valid_data_out_dmL1_0),		//salida valid dato 1 final
		      .valid_data_out_1	(valid_data_out_dmL1_1),		//salida valid dato 2 final
		      .valid_data_out_2	(valid_data_out_dmL1_2),		//salida valid dato 3 final
		      .valid_data_out_3	(valid_data_out_dmL1_3),		//salida valid dato 4 final
		      // Inputs
		      .clk_1		(clk_1),		//entra el cable del clk_1 salida del generador
		      .clk_2		(clk_2),		//entra el cable del clk_2 salida del generador
		      .clk_4		(clk_4),		//entra el cable del clk_4 salida del generador
		      .clk_16		(clk_16),		//entra el clk input del modulo phy
		      .reset_L		(reset_L),		//entra el reset_L input del modulo phy
		      .data_in		(data_in_s2p_1[1:0]));		//entra el flop con la salida del p2s, es decir, el dato en serial

    always @ (posedge clk_1)		//paso final de salida
    begin
        data_out_0 <= data_out_dmL1_0;
        data_out_1 <= data_out_dmL1_1;
        data_out_2 <= data_out_dmL1_2;
        data_out_3 <= data_out_dmL1_3;
        valid_data_out_0 <= valid_data_out_dmL1_0;
        valid_data_out_1 <= valid_data_out_dmL1_1;
        valid_data_out_2 <= valid_data_out_dmL1_2;
        valid_data_out_3 <= valid_data_out_dmL1_3;
    end

endmodule
