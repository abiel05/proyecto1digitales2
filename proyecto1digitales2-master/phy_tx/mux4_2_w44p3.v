// +FHDR------------------------------------------------------------
//                 Copyright (c) 2018 .
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : mux4_2_w44p3.v
// Author        : 
// Created On    : 2018-10-31 17:39
// Last Modified : 
// -----------------------------------------------------------------
// Description:
//            n-intento de lograr un mux 4 a 2
//
// -FHDR------------------------------------------------------------


`timescale 1ns/100ps

module mux4_2_w44p3(
    input clk_f,
    input clk_2f,
    input clk_4f,
    input reset_L,
    input [7:0] data_0,
    input [7:0] data_1,
    input [7:0] data_2,
    input [7:0] data_3,
    input valid_data_0,
    input valid_data_1,
    input valid_data_2,
    input valid_data_3,
    output reg [7:0] data_out_0,
    output reg [7:0] data_out_1,
    output reg valid_data_out_0,
    output reg valid_data_out_1
);

reg [7:0] in_data_0, in_data_1, in_data_2, in_data_3;
reg in_valid_data_0, in_valid_data_1, in_valid_data_2, in_valid_data_3;
wire selector;

wire[7:0] mux_0_data, mux_1_data;
wire mux_0_valid, mux_1_valid;

reg [7:0] mux_out_data_0, mux_out_data_1;
reg valid_mux_out_data_0, valid_mux_out_data_1;

always @(posedge clk_f)
begin
    if(reset_L)
    begin
        in_data_0 <= data_0;
        in_data_1 <= data_1;
        in_data_2 <= data_2;
        in_data_3 <= data_3;
        in_valid_data_0 <= valid_data_0;
        in_valid_data_1 <= valid_data_1;
        in_valid_data_2 <= valid_data_2;
        in_valid_data_3 <= valid_data_3;
    end
    else
    begin
        in_data_0 <= 8'b0;
        in_data_1 <= 8'b0;
        in_data_2 <= 8'b0;
        in_data_3 <= 8'b0;
        in_valid_data_0 <= 1'b0;
        in_valid_data_1 <= 1'b0;
        in_valid_data_2 <= 1'b0;
        in_valid_data_3 <= 1'b0;
    end
end

assign selector = clk_f;
assign mux_0_data = (selector)?in_data_1:in_data_0;
assign mux_1_data = (selector)?in_data_3:in_data_2;
assign mux_0_valid = (selector)?in_valid_data_1:in_valid_data_0;
assign mux_1_valid = (selector)?in_valid_data_3:in_valid_data_2;

/*always @(posedge clk_f)
begin
    if(reset_L)
    begin
       mux_out_data_0 <= mux_0_data;
       mux_out_data_1 <= mux_1_data;
       valid_mux_out_data_0 <= mux_0_valid;
       valid_mux_out_data_1 <= mux_1_valid; 
    end
    else
    begin
       mux_out_data_0 <= 8'b0;
       mux_out_data_1 <= 8'b0;
       valid_mux_out_data_0 <= 1'b0;
       valid_mux_out_data_1 <= 1'b0; 
    end
end
*/
always @(posedge clk_2f)
begin
    data_out_0 <= mux_0_data;
    data_out_1 <= mux_1_data;
    valid_data_out_0 <= mux_0_valid;
    valid_data_out_1 <= mux_1_valid; 
end

endmodule
