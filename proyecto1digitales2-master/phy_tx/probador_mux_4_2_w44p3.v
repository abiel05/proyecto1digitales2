// +FHDR------------------------------------------------------------
//                 Copyright (c) 2018 .
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : probador_mux_4_2_w44p3.v
// Author        : 
// Created On    : 2018-10-31 18:01
// Last Modified : 
// -----------------------------------------------------------------
// Description:
//
//
// -FHDR------------------------------------------------------------

`timescale 1ns/100ps

module probador_mux_4_2_w44p3(
    output reg clk_16f,
    output reg reset_L,
    output reg reset_clk,
    output reg [7:0] data_0,
    output reg [7:0] data_1,
    output reg [7:0] data_2,
    output reg [7:0] data_3,
    output reg valid_data_0,
    output reg valid_data_1,
    output reg valid_data_2,
    output reg valid_data_3,
    input [7:0] data_out_0,
    input [7:0] data_out_1,
    input valid_data_out_0,
    input valid_data_out_1
);

// by  2018-10-31 | reg compare;
// by  2018-10-31 | reg [15:0] conductual, estructural;

    //Inicialización de registros de salida
    initial
    begin
        clk_16f = 0;
        reset_L = 0;
        reset_clk = 0;
        data_0[7:0] = 0;
        data_1[7:0] = 0;
        data_2[7:0] = 0;
        data_3[7:0] = 0;
        valid_data_0 = 0;
        valid_data_1 = 0;
        valid_data_2 = 0;
        valid_data_3 = 0;
// by  2018-10-31 |         compare = 0;
// by  2018-10-31 |         conductual[15:0] = 16'b0;
// by  2018-10-31 |         estructural[15:0] = 16'b0;
    end

    //Reloj base a 16f
    always
    begin
	#1 clk_16f = ~clk_16f;
    end


    //Asignación registros de salida
    always @(*)
    begin
        $dumpfile("mux_4_2.vcd");
        $dumpvars;
        $display ("Inicio Simulación!!");
        $monitor ($time,"\tclk = %b, data_out = %h", clk_16f, data_out_1[7:0]);
        reset_L = 0;
        #100 reset_clk = 1;
        #2000 valid_data_0 = 1;
            valid_data_1 = 1;
            valid_data_2 = 1;
            valid_data_3 = 1;
        #5000 reset_L = 1;
        #2000 valid_data_0 = 0;
            valid_data_1 = 0;
            valid_data_2 = 0;
            valid_data_3 = 0;
        #2000 valid_data_0 = 1;
            valid_data_1 = 1;
            valid_data_2 = 1;
            valid_data_3 = 1;
        #4000 data_0[7:0] = 8'h8;
            data_2[7:0] = 8'hE;
            data_3[7:0] = 8'h6;
        #2000 valid_data_0 = 1;
            valid_data_2 = 1;
        #2000 valid_data_0 = 0;
        #2000 valid_data_0 = 1;
            valid_data_3 = 1;
            data_1[7:0] = 8'hF;
        #2000 valid_data_1 = 1;
        #2000 valid_data_1 = 0;
        #2000 valid_data_1 = 1;
        #10000 data_0[7:0] = 8'hE;
            data_1[7:0] = 8'h7;
            data_2[7:0] = 8'h3;
            data_3[7:0] = 8'hA;
        #20000 reset_L = 0;
        #5000 data_0[7:0] = 8'hA;
            data_1[7:0] = 8'h5;
        #5000 reset_L = 1;
        #10000 data_0[7:0] = 8'h6;
            data_1[7:0] = 8'hC;
        #2000 valid_data_1 = 1;
        #2000 valid_data_1 = 0;
        #2000 valid_data_1 = 1;
        #2000 valid_data_0 = 1;
        #2000 valid_data_0 = 0;
        #2000 valid_data_0 = 1;
        #20000 $finish;
    end

endmodule
