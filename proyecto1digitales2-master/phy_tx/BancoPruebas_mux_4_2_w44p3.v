// +FHDR------------------------------------------------------------
//                 Copyright (c) 2018 .
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : BancoPruebas_mux_4_2_w44p3.v
// Author        : 
// Created On    : 2018-10-31 18:10
// Last Modified : 
// -----------------------------------------------------------------
// Description:
//
//
// -FHDR------------------------------------------------------------

`include "mux4_2_w44p3.v"
`include "probador_mux_4_2_w44p3.v"
`timescale 1ns/100ps

module BancoPruebas_mux_4_2_w44p3();


    wire [7:0] data_0, data_1, data_2, data_3;
    wire valid_data_0, valid_data_1, valid_data_2, valid_data_3;
    wire clk_f, clk_2f, clk_4f, clk_16f;
    wire reset_L, reset_clk;
    wire [7:0] data_out_0, data_out_1;
    wire valid_data_out_0, valid_data_out_1;

    greloj_cond clks (/*autoinst*/
		      // Outputs
		      .clk_4		(clk_4f),
		      .clk_2		(clk_2f),
		      .clk_1		(clk_f),
		      // Inputs
		      .reset_L		(reset_clk),
		      .clk_in		(clk_16f));

probador_mux_4_2_w44p3 pm42 (/*autoinst*/
			     // Outputs
			     .clk_16f		(clk_16f),
			     .reset_L		(reset_L),
			     .reset_clk		(reset_clk),
			     .data_0		(data_0[7:0]),
			     .data_1		(data_1[7:0]),
			     .data_2		(data_2[7:0]),
			     .data_3		(data_3[7:0]),
			     .valid_data_0	(valid_data_0),
			     .valid_data_1	(valid_data_1),
			     .valid_data_2	(valid_data_2),
			     .valid_data_3	(valid_data_3),
			     // Inputs
			     .data_out_0	(data_out_0[7:0]),
			     .data_out_1	(data_out_1[7:0]),
			     .valid_data_out_0	(valid_data_out_0),
			     .valid_data_out_1	(valid_data_out_1));

mux4_2_w44p3 m42 (/*autoinst*/
		  // Outputs
		  .data_out_0		(data_out_0[7:0]),
		  .data_out_1		(data_out_1[7:0]),
		  .valid_data_out_0	(valid_data_out_0),
		  .valid_data_out_1	(valid_data_out_1),
		  // Inputs
		  .clk_f			(clk_f),
		  .clk_2f			(clk_2f),
		  .clk_4f			(clk_4f),
		  .reset_L		(reset_L),
		  .data_0		(data_0[7:0]),
		  .data_1		(data_1[7:0]),
		  .data_2		(data_2[7:0]),
		  .data_3		(data_3[7:0]),
		  .valid_data_0		(valid_data_0),
		  .valid_data_1		(valid_data_1),
		  .valid_data_2		(valid_data_2),
		  .valid_data_3		(valid_data_3));

endmodule
