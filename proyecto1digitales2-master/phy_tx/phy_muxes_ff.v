////////////////////////////////////////////////////////////////////////////////
//Definición módulo PCIe transmisor phy_tx 
////////////////////////////////////////////////////////////////////////////////
`include "muxL1.v"
// by  2018-10-30 | `include "paralelo_a_serial.v"
`timescale 1ns/100ps
module phy_tx( //  2018-10-17 | //Declaración de variables entrada-salida
    input [7:0] data_0, 
    input [7:0] data_1,
    input [7:0] data_2,
    input [7:0] data_3,
    input valid_data_0,
    input valid_data_1,
    input valid_data_2,
    input valid_data_3,
    input clk_f,
    input clk_2f,
    input clk_4f,
    input clk_16f,
    input reset_L,
// by  2018-10-30 |     output [1:0] data_out_p2s);

     output reg [7:0] data_in_mL2_0,
     output reg [7:0] data_in_mL2_1,
     output reg valid_data_in_mL2_0,
     output reg valid_data_in_mL2_1);

// by  2018-10-18 | // Declaración de variables internas 
// by  2018-10-18 | Etapa flops @clk_f 
    reg [7:0] data_in_mL1_0, data_in_mL1_1, data_in_mL1_2, data_in_mL1_3;
    reg valid_data_in_mL1_0, valid_data_in_mL1_1, valid_data_in_mL1_2, valid_data_in_mL1_3; 
// by  2018-10-17 | Mux L1 @ clk_2f 
    wire [7:0] data_out_mL1_0, data_out_mL1_1;
    wire valid_data_out_mL1_0, valid_data_out_mL1_1;
// by  2018-10-17 | Etapa flops @clk_2f 
// by  2018-10-30 |     reg [7:0] data_in_mL2_0, data_in_mL2_1; 
// by  2018-10-30 |     reg valid_data_in_mL2_0, valid_data_in_mL2_1;
// by  2018-10-17 | Mux L2 @ clk_4f 
// by  2018-10-30 |     wire [7:0] data_out_mL2;
// by  2018-10-30 |     wire valid_data_out_mL2_0;

// by  2018-10-17 | Etapa flops @clk_f 
    always @(posedge clk_f)
    begin
       data_in_mL1_0[7:0] <= data_0[7:0]; 
       data_in_mL1_1[7:0] <= data_1[7:0]; 
       data_in_mL1_2[7:0] <= data_2[7:0]; 
       data_in_mL1_3[7:0] <= data_3[7:0]; 
       valid_data_in_mL1_0<=valid_data_0;
       valid_data_in_mL1_1<=valid_data_1;
       valid_data_in_mL1_2<=valid_data_2;
       valid_data_in_mL1_3<=valid_data_3;
    end

// by  2018-10-17 | Mux L1 @ clk_2f 
    muxL1 mL1_0 (/*autoinst*/
    .clk                            (clk_16f                                     ), //input
    .reset_L                        (reset_L                                    ), // input 
    .data_0                         (data_in_mL1_0[7:0]                                ), // input 
    .valid_data_0                   (valid_data_in_mL1_0                               ), // input 
    .data_1                         (data_in_mL1_1[7:0]                                ), // input 
    .valid_data_1                   (valid_data_in_mL1_1                               ), // input 
    .data_2                         (data_in_mL1_2[7:0]                                ), // input 
    .valid_data_2                   (valid_data_in_mL1_2                               ), // input 
    .data_3                         (data_in_mL1_3[7:0]                                ), // input 
    .valid_data_3                   (valid_data_in_mL1_3                               ), // input 
    .valid_data_out_0               (valid_data_out_mL1_0                           ), // output
    .data_out_0                     (data_out_mL1_0[7:0]                            ), // output
    .valid_data_out_1               (valid_data_out_mL1_1                           ), // output
    .data_out_1                     (data_out_mL1_1[7:0]                            )  // output

        //Instanciando 2 módulos Multiplexor 2:1 con selector automático para formar 1 Multiplexor 4:2 con selector automático.
);

// by  2018-10-17 | Etapa flops @clk_2f 
    always @(posedge clk_16f)
    begin
       data_in_mL2_0[7:0] <= data_out_mL1_0[7:0]; 
       data_in_mL2_1[7:0] <= data_out_mL1_1[7:0]; 
       valid_data_in_mL2_0<=valid_data_out_mL1_0;
       valid_data_in_mL2_1<=valid_data_out_mL1_1;
    end

endmodule
