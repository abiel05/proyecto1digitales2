`timescale 1ns /100ps
module BancoPruebas_p2_mapped();
    wire clk, valid_data_in, reset_L;
    wire [7:0] data_in;
    wire [1:0] data_out;

    probador_p2s_mapped p0_m (.clk(clk),
	    .reset_L(reset_L),
	    .data_in(data_in[7:0]),
	    .valid_data_in(valid_data_in),
	    .data_out(data_out[1:0]));

    sintetizado p2s_0_m (.clk(clk),
	    .reset_L(reset_L),
	    .data_in(data_in[7:0]),
	    .valid_data_in(valid_data_in),
	    .data_out(data_out[1:0]));

endmodule
