////////////////////////////////////////////////////////////////////////////////
//Definición modulo de verificación
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module probador_p2s_mapped (input [1:0] data_out,
    output reg clk,
    output reg reset_L,
    output reg [7:0] data_in,
    output reg valid_data_in);

    initial
    begin
        data_in[7:0]=8'h0;
        valid_data_in = 0;
        clk = 0;
        reset_L = 0;
    end

    //Reloj base a 16f
    always
    begin
        #1 clk = ~clk;
    end

    //Asignación registros de salida
    always @(*)
    begin
        $dumpfile("p2s_mapped.vcd");
        $dumpvars;
        $display ("Inicio Simulación!!");
        $monitor ($time,"\tclk = %b, data_in = %h, data_out_cond = %h, data_out_est = %h", clk, data_in, data_out, data_out);
        #5 data_in[7:0] = 8'hFF;
        #5 valid_data_in = 1;
        #5 reset_L = 1'b1;
        #5 data_in[7:0] = 8'h00;
        #5 data_in[7:0] = 8'hA8;
        #5 data_in[7:0] = 8'hDD;
        #5 valid_data_in = 0;
        #5 data_in[7:0] = 8'hDD;
        #5 data_in[7:0] = 8'hE1;
        #5 data_in[7:0] = 8'h05;
        #5 valid_data_in = 1;
        #5 data_in[7:0] = 8'h04;
        #5 data_in[7:0] = 8'h45;
        #20 $finish;
    end

endmodule
