////////////////////////////////////////////////////////////////////////////////
//Definición módulo de verificación del PCIe phy
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module probador_phy (
	output reg clk_16,
	output reg reset_L,
	output reg reset_clk,		//se agrega el reset exclusivo para el modulo generador de relojes
	output reg [7:0] data_in_0,
	output reg [7:0] data_in_1,
	output reg [7:0] data_in_2,
	output reg [7:0] data_in_3,
	output reg valid_data_in_0,
	output reg valid_data_in_1,
	output reg valid_data_in_2,
	output reg valid_data_in_3,
	input [7:0] data_out_0_cond,
	input [7:0] data_out_0_estr,
	input [7:0] data_out_1_cond,
	input [7:0] data_out_1_estr,
	input [7:0] data_out_2_cond,
	input [7:0] data_out_2_estr,
	input [7:0] data_out_3_cond,
	input [7:0] data_out_3_estr,
	input valid_data_out_0_cond,
	input valid_data_out_0_estr,
	input valid_data_out_1_cond,
	input valid_data_out_1_estr,
	input valid_data_out_2_cond,
	input valid_data_out_2_estr,
	input valid_data_out_3_cond,
	input valid_data_out_3_estr);		//Declaración de variables entrada-salida

    //Declaración de variables internas
//    reg compare;
//    reg [1:0] conductual, estructural;

    //Inicialización de registros de salida
    initial
    begin
        clk_16 = 0;
        reset_L = 0;
        reset_clk = 0;
        data_in_0[7:0] = 0;
        data_in_1[7:0] = 0;
        data_in_2[7:0] = 0;
        data_in_3[7:0] = 0;
        valid_data_in_0 = 0;
        valid_data_in_1 = 0;
        valid_data_in_2 = 0;
        valid_data_in_3 = 0;
//        compare = 0;
//        conductual[1:0] = 2'b0;
//        estructural[1:0] = 2'b0;
    end


    //Reloj base a 16f
    always
    begin
	#1 clk_16 = ~clk_16;
    end

    //Asignación registros de salida
    initial
    begin
        $dumpfile("phy.vcd");
        $dumpvars;
        $display ("Inicio Simulación!!");
//        $monitor ($time,"\tclk = %b, data_out_cond = %h, data_out_est = %h", clk_16, data_out_cond, data_out_est);
        reset_L = 0;
        #100 reset_clk = 1;
        #2000 valid_data_in_0 = 1;
            valid_data_in_1 = 1;
            valid_data_in_2 = 1;
            valid_data_in_3 = 1;
        #5000 reset_L = 1;
        #2000 valid_data_in_0 = 0;
            valid_data_in_1 = 0;
            valid_data_in_2 = 0;
            valid_data_in_3 = 0;
        #2000 valid_data_in_0 = 1;
            valid_data_in_1 = 1;
            valid_data_in_2 = 1;
            valid_data_in_3 = 1;
        #4000 data_in_0[7:0] = 8'h8;
            data_in_2[7:0] = 8'hE;
            data_in_3[7:0] = 8'h6;
        #2000 valid_data_in_0 = 1;
            valid_data_in_2 = 1;
        #2000 valid_data_in_0 = 0;
        #2000 valid_data_in_0 = 1;
            valid_data_in_3 = 1;
            data_in_1[7:0] = 8'hF;
        #2000 valid_data_in_1 = 1;
        #2000 valid_data_in_1 = 0;
        #2000 valid_data_in_1 = 1;
        #10000 data_in_0[7:0] = 8'hE;
            data_in_1[7:0] = 8'h7;
            data_in_2[7:0] = 8'h3;
            data_in_3[7:0] = 8'hA;
        #20000 reset_L = 0;
        #5000 data_in_0[7:0] = 8'hA;
            data_in_1[7:0] = 8'h5;
        #5000 reset_L = 1;
        #10000 data_in_0[7:0] = 8'h6;
            data_in_1[7:0] = 8'hC;
        #2000 valid_data_in_1 = 1;
        #2000 valid_data_in_1 = 0;
        #2000 valid_data_in_1 = 1;
        #2000 valid_data_in_0 = 1;
        #2000 valid_data_in_0 = 0;
        #2000 valid_data_in_0 = 1;
        #20000 $finish;
    end

    //Asignación de salidas de los multiplexores a registros internos de probador.v para comparación
//    always @(posedge clk_16)
//    begin
//        conductual[1:0] <= data_out_cond[1:0];
//        estructural[1:0] <= data_out_est[1:0];
//    end

	//Lógica de comparación
//    always @(*)
//    begin
//        compare = 0;
//        if(conductual != estructural)
//        begin
//            compare = 1;
//        end
//    end

endmodule
