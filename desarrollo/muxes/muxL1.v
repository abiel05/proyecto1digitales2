////////////////////////////////////////////////////////////////////////////////
//Definición módulo Multiplexor 4:2 con selector automático
////////////////////////////////////////////////////////////////////////////////
`include "muxL2.v"
`timescale 1ns /100ps
module muxL1 (
    input clk_f,
    input clk_2f,
    input reset_L,
    input [7:0] data_0,
    input [7:0] data_1,
    input [7:0] data_2,
    input [7:0] data_3,
    input valid_data_0,
    input valid_data_1,
    input valid_data_2,
    input valid_data_3,
    output [7:0] data_out_0,
    output [7:0] data_out_1,
    output valid_data_out_0,
    output valid_data_out_1
);
    //Instanciando 2 módulos Multiplexor 2:1 con selector automático para formar 1 Multiplexor 4:2 con selector automático.
    muxL2 mux_0(
        .clk_f(clk_f),
        .clk_2f(clk_2f),
    .clk_sel                   (clk_f                                     ), //input
        .reset_L(reset_L),
        .data_0(data_0),
        .data_1(data_1),
        .data_out_mL2(data_out_0),
        .valid_data_0(valid_data_0),
        .valid_data_1(valid_data_1),
        .valid_data_out_mL2(valid_data_out_0));

    muxL2 mux_1(
        .clk_f(clk_f),
        .clk_2f(clk_2f),
    .clk_sel                   (clk_f                                     ), //input
        .reset_L(reset_L),
        .data_0(data_2),
        .data_1(data_3),
        .data_out_mL2(data_out_1),
        .valid_data_0(valid_data_2),
        .valid_data_1(valid_data_3),
        .valid_data_out_mL2(valid_data_out_1));

endmodule
