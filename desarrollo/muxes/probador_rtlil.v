////////////////////////////////////////////////////////////////////////////////
//Definición modulo de verificación
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module probador (output reg clk,
                 input [7:0] data_out_cond_0,
                 input [7:0] data_out_cond_1,
                 input [7:0] data_out_est_0,
                 input [7:0] data_out_est_1,
                 input valid_data_out_cond_0,
                 input valid_data_out_cond_1,
                 input valid_data_out_est_0,
                 input valid_data_out_est_1,
                 output reg valid_data_0,
                 output reg valid_data_1,
                 output reg valid_data_2,
                 output reg valid_data_3,
                 output reg reset_L,
                 output reg reset_clk,
                 output reg [7:0] data_0,
                 output reg [7:0] data_1,
                 output reg [7:0] data_2,
                 output reg [7:0] data_3);//Declaración de variables entrada-salida

    //Declaración de variables internas
    reg compare;
    reg [15:0] conductual, estructural;

    //Inicialización de registros de salida
    initial
    begin
        clk = 0;
        reset_L=0;
        reset_clk=0;
        data_0[7:0]=0;
        data_1[7:0]=0;
        data_2[7:0]=0;
        data_3[7:0]=0;
        valid_data_0=0;
        valid_data_1=0;
        valid_data_2=0;
        valid_data_3=0;
        compare=0;
        conductual[15:0]=16'h0;
        estructural[15:0]=16'h0;
    end

    //Reloj base a 16f
    always
    begin
	#1 clk = ~clk;
    end

    //Asignación registros de salida
    always @(*)
    begin
        $dumpfile("senalesVCD_rtlil_L1.vcd");
        $dumpvars;
        $display ("Inicio Simulación!!");
        $monitor ($time,"\tclk = %b, reset_L = %b, data_0 = %h, data_1 = %h, data_out = %h, data_out_est = %h", clk, reset_L, data_0, data_1, data_out_cond_0, data_out_est_0);
        #4 reset_clk = 1;
        #5000 reset_L = 1;
        #4000 data_0[7:0] = 8'h8;
        data_2[7:0] = 8'hE;
        data_3[7:0] = 8'h6;
        #2000 valid_data_0 = 1;
        valid_data_2 = 1;
        #2000 valid_data_0 = 0;
        #2000 valid_data_0 = 1;
        valid_data_3 = 1;
        data_1[7:0] = 8'hF;
        #2000 valid_data_1 = 1;
        #2000 valid_data_1 = 0;
        #2000 valid_data_1 = 1;
        #10000 data_0[7:0] = 8'hE;
        data_1[7:0] = 8'h7;
        data_2[7:0] = 8'h3;
        data_3[7:0] = 8'hA;
        #20000 reset_L = 0;
        #5000 data_0[7:0] = 8'hA;
        data_1[7:0] = 8'h5;
        #5000 reset_L = 1;
        #10000 data_0[7:0] = 8'h6;
        data_1[7:0] = 8'hC;
        #2000 valid_data_1 = 1;
        #2000 valid_data_1 = 0;
        #2000 valid_data_1 = 1;
        #2000 valid_data_0 = 1;
        #2000 valid_data_0 = 0;
        #2000 valid_data_0 = 1;
        #20000 $finish;
    end

    //Asignación de salidas de los multiplexores a registros internos de probador.v para comparación
    always @(posedge clk)
    begin
        conductual[15:0] <= {data_out_cond_1[7:0],data_out_cond_0[7:0]};
        estructural[15:0] <= {data_out_est_1[7:0],data_out_est_0[7:0]};
    end
    //Lógica de comparación
    always @(*)
    begin
        compare = 0;
        if(conductual != estructural)
        begin
            compare = 1;
        end
    end

endmodule
