////////////////////////////////////////////////////////////////////////////////
//Definición módulo Multiplexor 2:1 con selector automático
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module mux (input clk,
            input reset_L,
            input [3:0] data_0,
            input valid_data_0,
            input valid_data_1,
            input [3:0] data_1,
            output reg valid_data_out,
            output reg [3:0] data_out);//Declaración de variables entrada-salida

    //Declaración de variables internas
    reg selector;
    reg valid_out, valid_both, valid_one, valid_select;
    reg [3:0] data_entry_select, data_valid_select, data_entry_valid, data_valid_out;

    //Lógica de salida data_out
    always @(posedge clk)
    begin
        data_out[3:0]<=0;
        selector<=0;
        valid_data_out<=0;
        if(reset_L == 1)
        begin
            data_out[3:0]<=data_valid_out[3:0];
            selector<=~selector;
            valid_data_out<=valid_out;
        end
    end

    always @(*)
    begin
        valid_out = valid_data_0 | valid_data_1;
        valid_both = valid_data_0 & valid_data_1;
        valid_one = valid_data_0 ^ valid_data_1;
        valid_select = valid_one & valid_data_1;
        if(selector == 1)
        begin
            data_entry_select[3:0] = data_1[3:0];
        end
        else
        begin
            data_entry_select[3:0] = data_0[3:0];
        end
        if(valid_select == 1)
        begin
            data_valid_select[3:0] = data_1[3:0];
        end
        else
        begin
            data_valid_select[3:0] = data_0[3:0];
        end
        if(valid_both == 1)
        begin
            data_entry_valid[3:0] = data_entry_select[3:0];
        end
        else
        begin
            data_entry_valid[3:0] = data_valid_select[3:0];
        end
        if(valid_out == 1)
        begin
            data_valid_out[3:0] = data_entry_valid[3:0];
        end
        else
        begin
            data_valid_out[3:0] = data_out[3:0];
        end
    end

endmodule
