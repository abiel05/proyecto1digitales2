`timescale 1ns /100ps
module serial_a_paralelo (input clk,
    input reset_L,
    input [1:0] data_in,
    output reg valid_data_out,
    output reg [7:0] data_out);

    reg [1:0] contador_com;
    reg [1:0] contador_serial;
    reg [7:0] temporal;
    reg active;


    //Bloque de lógica secuencial
    always @(posedge clk)
    begin
        if (reset_L == 0)
        begin
            valid_data_out <= 1;
            data_out[7:0] <= 2'b0;
            contador_serial <= 2'b0;
            valid_data_out <= 1'b0;
            contador_com[1:0] <= 2'b0;
        end
        else
        begin
            if(contador_serial<=2)
            begin
                data_out[7:0] <= data_out[7:0];
                contador_serial <= contador_serial+1;
            end
            else
            begin
                data_out[7:0] <= temporal[7:0];
                contador_serial <= 2'b0;
                if(temporal[7:0] == 8'hBC)
                begin
                    contador_com <= contador_com + 1;
                end
                else
                begin
                    contador_com[1:0] <= 2'b0;
                end
                if(contador_com>=3)
                begin
                    active <= 1'b1;
                    valid_data_out = 1;
                end
                else
                begin
                    active <= 1'b0;
                end
            end
        end
    end

    //Bloque de lógica combinacional
    always @(contador_serial)
    begin
	case (contador_serial)
	    0: temporal[1:0]=data_in[1:0];
	    1: temporal[3:2]=data_in[1:0];
	    2: temporal[5:4]=data_in[1:0];
	    default: temporal[7:6]=data_in[1:0];
	endcase
    end

endmodule
