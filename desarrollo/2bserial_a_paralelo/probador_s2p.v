////////////////////////////////////////////////////////////////////////////////
//Definición módulo de verificación
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module probador_s2p (input [7:0] data_out,
    input valid_data_out,
    output reg clk,
    output reg reset_L,
    output reg [1:0] data_in);

    initial
    begin
	data_in[1:0]=2'h0;
	clk = 0;
	reset_L = 0;
    end

    //Reloj base a 16f
    always
    begin
	#1 clk = ~clk;
    end

    //Asignación registros de salida
    always @(*)
    begin
        $dumpfile("s2p.vcd");
        $dumpvars;
        $display ("Inicio Simulación!!");
        $monitor ($time,"\tclk = %b, data_in = %h, data_out_cond = %h, data_out_est = %h", clk, data_in, data_out, data_out);
	#5 reset_L = 1'b1;
        #5 data_in[1:0] = 2'b11;
        #5 data_in[1:0] = 2'b00;
        #5 data_in[1:0] = 2'b10;
        #5 data_in[1:0] = 2'b01;
        #5 data_in[1:0] = 2'b01;
        #5 data_in[1:0] = 2'b10;
        #5 data_in[1:0] = 2'b11;
        #5 data_in[1:0] = 2'b01;
        #5 data_in[1:0] = 2'b00;

        #25 data_in[1:0] = 2'b00;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b10;
        #2 data_in[1:0] = 2'b00;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b10;
        #2 data_in[1:0] = 2'b00;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b10;
        #2 data_in[1:0] = 2'b00;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b10;
        #2 data_in[1:0] = 2'b00;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b10;
        #2 data_in[1:0] = 2'b00;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b10;
        #2 data_in[1:0] = 2'b00;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b10;
        #2 data_in[1:0] = 2'b00;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b00;
        #2 data_in[1:0] = 2'b10;
        #2 data_in[1:0] = 2'b01;
        #2 data_in[1:0] = 2'b01;
        #2 data_in[1:0] = 2'b10;
        #2 data_in[1:0] = 2'b11;
        #2 data_in[1:0] = 2'b01;
        #2 data_in[1:0] = 2'b00;
	#20 $finish;
    end

endmodule
