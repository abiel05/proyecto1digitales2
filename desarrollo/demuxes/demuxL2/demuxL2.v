////////////////////////////////////////////////////////////////////////////////
//Definición módulo demux 2:1 con selector automático
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module demuxL2 (
	input clk_f0,
	input clk_2f0,
	input reset_L,
	output reg [7:0] data_0,
// by  2018-11-20 | 	output reg [7:0] data_0,
	output reg valid_data_0,
// by  2018-11-20 | 	output reg valid_data_0,
	output reg valid_data_1,
	output reg [7:0] data_1,
	input valid_data_in,
	input [7:0] data_in);//Declaración de variables entrada-salida

    //Declaración de variables internas
    reg selector;
	reg [7:0] flop0_data, flop1_data, flop2_data1;
	reg flop0_valid, flop1_valid;//, flop2_valid_data1; 
// by  2018-11-20 | 	reg [7:0] flop_data_in;
// by  2018-11-20 | 	reg flop_valid_in;

// by  2018-11-20 | 	assign selector = clk_f0;
    always @(posedge clk_2f0)
    begin
	    selector <= clk_f0;
	end

/*
    always @(posedge clk_2f)
    begin
// by  2018-11-20 | 	    selector <= clk_f;
		if(reset_L)
		begin
			flop_data_in<=data_in;
			flop_valid_in<=valid_data_in;
		end
		else
		begin
			flop_data_in<=8'b0;
			flop_valid_in<=0;
		end
	end
*/



/*
    //Lógica de salida data_0 y data_1
    always @(posedge clk_2f)
    begin
        if(reset_L)
        begin
                    flop1_data[7:0] <= flop1_data[7:0];  //  mantiene el valor anterior de dato 1
                    flop0_data[7:0] <= flop0_data[7:0];  //  mantiene el valor anterior de dato 0
                    flop1_valid <= flop1_valid;  //  mantiene el valor anterior de valid dato 1
                    flop0_valid <= flop0_valid;  //  mantiene el valor anterior de valid dato 1
            if(selector)  //  si el selector esta en 1:
            begin
                if(flop_valid_in)  // si el dato es valido:
                begin
                    flop1_data[7:0] <= flop_data_in[7:0];  //  pone el dato de out en data 1
                    flop1_valid <= 1'b1;  //  pone valido el dato
                end
                else  //  si no es valido:
                begin
                    flop1_data[7:0] <= flop1_data[7:0];  //  mantiene el valor anterior de dato 1
                    flop1_valid <= 1'b0;  //  pone no valido el dato
                end
            end
            else  //  si el selector esta en 0:
            begin
                if(flop_valid_in)  //  si el dato es valido:
                begin
// by  2018-11-20 |                     flop0_data[7:0] <= flop_data_in[7:0];  //  pone el dato de out en data 0
// by  2018-11-20 |                     flop0_valid <= 1'b1;  //  pone valido el dato
                    data_0[7:0] <= flop_data_in[7:0];  //  pone el dato de out en data 0
                    valid_data_0 <= 1'b1;  //  pone valido el dato
                end
                else  //  si no es valido:
                begin
// by  2018-11-20 |                     flop0_data[7:0] <= flop0_data[7:0];  //  mantiene el valor anterior de dato 0
// by  2018-11-20 |                     flop0_valid <= 1'b0;  //  pone no valido el dato
                    data_0[7:0] <= data_0[7:0];  //  mantiene el valor anterior de dato 0
                    valid_data_0 <= 1'b0;  //  pone no valido el dato
                end
            end
        end
        else
        begin
// by  2018-11-20 |             flop0_data[7:0]<=0;
            data_0[7:0]<=0;
            flop1_data[7:0]<=0;
// by  2018-11-20 |             flop0_valid<=0;
            flop1_valid<=0;
			valid_data_0<=0;
        end
    end*/

// by  2018-11-20 | 	assign data_0 = flop0_data;
// by  2018-11-20 | 	assign valida_data_0 = flop0_valid;

// by  2018-11-20 |     assign flop0_data = (!selector)?data_in:flop0_data;
// by  2018-11-20 |     assign flop1_data = (selector)?data_in:flop1_data;
// by  2018-11-20 | 	assign flop0_valid = (reset_L && valid_data_in)?1:0;
// by  2018-11-20 | 	assign flop1_valid = (reset_L && valid_data_in)?1:0;

	always @(posedge clk_2f0)
	begin
	    flop1_data<=flop1_data;
	    flop0_data<=flop0_data;
		flop1_valid<=flop1_valid;
		flop0_valid<=flop0_valid;
        if (selector) 
		begin
			flop1_data<=data_in;
			if (valid_data_in) flop1_valid <=1;
			else flop1_valid <=0;
		end
		else
		begin
			flop0_data<=data_in;
			if (valid_data_in) flop0_valid <=1;
			else flop0_valid <=0;
		end
// by  2018-11-20 | 		if (valid_data_in) 
// by  2018-11-20 | 		begin
// by  2018-11-20 | 			flop1_valid <=1;
// by  2018-11-20 | 			flop0_valid <=1;
// by  2018-11-20 | 		end
// by  2018-11-20 | 		else
// by  2018-11-20 | 		begin
// by  2018-11-20 | 			flop1_valid <=0;
// by  2018-11-20 | 			flop0_valid <=0;
// by  2018-11-20 | 		end
	end


// by  2018-11-19 | 	assign flop1_data[7:0] = (reset_L)?8'b0:(selector)?data_in[7:0]:flop1_data[7:0];

    always @(posedge clk_f0)
	begin
	    data_0 <= flop0_data;
// by  2018-11-20 | 		flop2_data1 <= flop1_data;
// by  2018-11-20 | 		data_1 <= flop2_data1;
		data_1 <= flop1_data;
		valid_data_0 <= flop0_valid;
// by  2018-11-20 | 		flop2_valid_data1 <= flop1_valid; 
// by  2018-11-20 | 		valid_data_1 <= flop2_valid_data1; 
		valid_data_1 <= flop1_valid; 
	end
endmodule
