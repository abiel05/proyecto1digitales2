////////////////////////////////////////////////////////////////////////////////
//Definición módulo de Banco de Pruebas
////////////////////////////////////////////////////////////////////////////////
`include "demuxL2.v"
`include "demuxL2_estructural.v"
`include "cmos_cells.v"
`include "demuxL2_p.v"
`include "greloj_cond.v"
`timescale 1ns /100ps
module demuxL2_tb();

    //Declaración de variables internas
    wire valid_data_0_cond,valid_data_0_estr,valid_data_1_cond,valid_data_1_estr,valid_data_in;
    wire [7:0] data_0_cond,data_0_estr,data_1_cond,data_1_estr,data_in;

    wire clk_f, clk_2f, clk_4f, clk_16f;
    wire reset_L, reset_clk;
    //Instanciando módulos Reloj y Probador.
    greloj_cond clks (/*autoinst*/
    .clk_8                          (clk_8f                                      ), // output
    .clk_4                          (clk_4f                                      ), // output
    .clk_2                          (clk_2f                                      ), // output
    .clk_1                          (clk_f                                      ), // output
    .reset_L                        (reset_clk                                     ), // input  // INST_NEW
    .clk_in                         (clk_16f                                     )  // input 

    // Salida de 4 ciclos reloj
    // 16 / 4 = 4 ciclos reloj
);
    //Instanciando módulo Probador.
    demuxL2_p probador(
	        .clk(clk_16f),
            .reset_L(reset_L),
            .reset_clk(reset_clk),
            .data_0_estr(data_0_estr),
            .valid_data_0_estr(valid_data_0_estr),
            .data_0_cond(data_0_cond),
            .valid_data_0_cond(valid_data_0_cond),
            .data_1_estr(data_1_estr),
            .valid_data_1_estr(valid_data_1_estr),
            .data_1_cond(data_1_cond),
            .valid_data_1_cond(valid_data_1_cond),
            .data_in(data_in),
            .valid_data_in(valid_data_in));

    //Instanciando módulo demux 4:2 con selector automático. Diseño conductual
    demuxL2 conductual(
	        .clk_f0(clk_4f),
	        .clk_2f0(clk_8f),
            .reset_L(reset_L),
            .data_0(data_0_cond),
            .valid_data_0(valid_data_0_cond),
            .data_1(data_1_cond),
            .valid_data_1(valid_data_1_cond),
            .data_in(data_in),
            .valid_data_in(valid_data_in));

    //Instanciando módulo demux 4:2 con selector automático. Diseño estructural Mapeado
    demuxL2_estructural estructural(
	        .clk_f0(clk_4f),
	        .clk_2f0(clk_8f),
            .reset_L(reset_L),
            .data_0(data_0_estr),
            .valid_data_0(valid_data_0_estr),
            .data_1(data_1_estr),
            .valid_data_1(valid_data_1_estr),
            .data_in(data_in),
            .valid_data_in(valid_data_in));

endmodule
