////////////////////////////////////////////////////////////////////////////////
//Definición modulo de verificación
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
module demuxL2_p (output reg clk,
                output reg reset_L,
                output reg reset_clk,
                input [7:0] data_0_cond,
                input [7:0] data_0_estr,
                input valid_data_0_cond,
                input valid_data_0_estr,
                input valid_data_1_cond,
                input valid_data_1_estr,
                input [7:0] data_1_cond,
                input [7:0] data_1_estr,
                output reg valid_data_in,
                output reg [7:0] data_in);//Declaración de variables entrada-salida

    //Declaración de variables internas
//    reg compare;
    reg [7:0] conductual_data_0, conductual_data_1, estructural_data_0, estructural_data_1;

    //Inicialización de registros de salida
    initial
    begin
        reset_L=0;
        reset_clk=0;
        data_in[7:0]=0;
        valid_data_in=0;
        //compare=0;
        conductual_data_0[7:0]=8'b0;
        conductual_data_1[7:0]=8'b0;
        estructural_data_0[7:0]=8'b0;
        estructural_data_1[7:0]=8'b0;
    end

    //Reloj
    initial clk <= 0;
    always #1 clk <= ~clk;

    //Asignación registros de salida
    initial 
    begin
        $dumpfile("demuxL2.vcd");
        $dumpvars;
// by  2018-11-19 |         $display ("Inicio Simulación");
// by  2018-11-19 |         $monitor ($time,"\tclk = %b, reset_L = %b, data_0_cond = %h, data_0_estr = %h, data_1_cond = %h, data_1_estr = %h, data_in = %h", clk, reset_L, data_0_cond, data_0_estr, data_1_cond, data_1_estr, data_in);
// by  2018-11-19 | 		#50 reset_clk = 0;
		#5 reset_clk = 1;
        #50 reset_L = 1;
        data_in[7:0] = 8'h8;
        //#4000 data_in[7:0] = 8'h8;
        #20 valid_data_in = 1;
		@(posedge clk);
        data_in[7:0] = 8'h8;
		@(posedge clk);	@(posedge clk);
        data_in[7:0] = 8'hA;
		@(posedge clk);	@(posedge clk);
        data_in[7:0] = 8'hF;
		@(posedge clk);	@(posedge clk);
        data_in[7:0] = 8'h2;
		@(posedge clk);	@(posedge clk);
        data_in[7:0] = 8'h0;
		@(posedge clk);	@(posedge clk);
        data_in[7:0] = 8'h1;
		@(posedge clk);	@(posedge clk);
        data_in[7:0] = 8'hA8;
		@(posedge clk);	@(posedge clk);
        data_in[7:0] = 8'h74;
		@(posedge clk);	@(posedge clk);
        data_in[7:0] = 8'hFF;
		@(posedge clk);	@(posedge clk);
        data_in[7:0] = 8'h25;
		@(posedge clk);	@(posedge clk);
        data_in[7:0] = 8'h14;
        #20 valid_data_in = 0;
        #20 valid_data_in = 1;
        data_in[7:0] = 8'hF;
        #20 valid_data_in = 1;
//        #2000 valid_data_in = 0;
        #10 valid_data_in = 1;
        #50 data_in[7:0] = 8'hE;
        valid_data_in = 0;
        #50 data_in[7:0] = 8'hE;
        valid_data_in = 1;
        #20 reset_L = 0;
        #50 data_in[7:0] = 8'hA;
        #50 reset_L = 1;
        #102 data_in[7:0] = 8'h6;
        #20 valid_data_in = 1;
        #20 valid_data_in = 0;
        #20 valid_data_in = 1;
        #20 data_in[7:0] = 8'hC;
        #20 valid_data_in = 0;
        #20 valid_data_in = 1;
        #100 $finish;
    end

    //Asignación de salidas de los demux a registros internos de probador.v para comparación
    always @(posedge clk)
    begin
        conductual_data_0[7:0] <= data_0_cond[7:0];
        conductual_data_1[7:0] <= data_1_cond[7:0];
        estructural_data_0[7:0] <= data_0_estr[7:0];
        estructural_data_1[7:0] <= data_1_estr[7:0];
    end

    //Lógica de comparación
//    always @(*)
//    begin
//        compare = 0;
//        if(conductual != estructural)
//        begin
//            compare = 1;
//        end
//    end

endmodule
