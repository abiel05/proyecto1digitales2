////////////////////////////////////////////////////////////////////////////////
//Definición módulo demux 4:2 con selector automático
////////////////////////////////////////////////////////////////////////////////
`timescale 1ns /100ps
//`include "demuxL2.v"
module demuxL1 (input clk,
            input reset_L,
            input [7:0] data_out_0,
            input valid_data_out_0,
            input [7:0] data_out_1,            
            input valid_data_out_1,
            output [7:0] data_0,
            output valid_data_0,
            output [7:0] data_1,
            output valid_data_1,
            output [7:0] data_2,
            output valid_data_2,
            output [7:0] data_3,
            output valid_data_3);//Declaración de variables entrada-salida

    //Instanciando 2 módulos demux 2:1 con selector automático para formar 1 demux 4:2 con selector automático.
    demuxL2 demux_0(.clk(clk),
                   .reset_L(reset_L),
                   .data_out(data_out_0),
                   .data_0(data_0),
                   .data_1(data_1),
                   .valid_data_out(valid_data_out_0),
                   .valid_data_0(valid_data_0),
                   .valid_data_1(valid_data_1));

    demuxL2 demux_1(.clk(clk),
                   .reset_L(reset_L),
                   .data_out(data_out_1),
                   .data_0(data_2),
                   .data_1(data_3),
                   .valid_data_out(valid_data_out_1),
                   .valid_data_0(valid_data_2),
                   .valid_data_1(valid_data_3));

endmodule
