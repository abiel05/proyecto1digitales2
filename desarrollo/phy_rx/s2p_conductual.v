`timescale 1ns /100ps
module s2p_conductual (
    input clk,
    input reset_L,
    input [1:0] data_in,
    output reg valid_data_out,
    output reg [7:0] data_out);

    //Definición variables internas
    reg [2:0] contador_com;
    reg [1:0] contador_serial;
    reg [1:0] temporal_0, temporal_1, temporal_2, temporal_3;
    reg [1:0] data_in_save;
    reg active;


    //Bloque de lógica secuencial
    always @(posedge clk)
    begin
        data_in_save[1:0]<=data_in[1:0];
        if (reset_L)
        begin
            contador_serial <= contador_serial+1;
            if(contador_serial<=2)
            begin
                data_out[7:0] <= data_out[7:0];
            end
            else
            begin
                data_out[7:0] <= {temporal_3[1:0],temporal_2[1:0],temporal_1[1:0],temporal_0[1:0]};
                if({temporal_3[1:0],temporal_2[1:0],temporal_1[1:0],temporal_0[1:0]}== 8'hBC)
                begin
                    contador_com <= contador_com + 1;
                end
                else
                begin
                    contador_com[1:0] <= 2'b0;
                end
                if(contador_com>=3)
                begin
                    active <= 1'b1;
                end
// by  2018-10-21 |                 else
// by  2018-10-21 |                 begin
// by  2018-10-21 |                     active <= 1'b0;
// by  2018-10-21 |                 end
                if((active==1) && ({temporal_3[1:0],temporal_2[1:0],temporal_1[1:0],temporal_0[1:0]}!=8'hBC))
                begin
                    valid_data_out<=1;
                end
            end
        end
        else
        begin
            valid_data_out <= 0;
            data_out[7:0] <= 8'b0;
            contador_serial[1:0] <= 2'b0;
            contador_com[2:0] <= 3'b0;
            active <= 0;
        end
    end

    //Bloque de lógica combinacional
// by  2018-10-21 |     always @(contador_serial)
// by  2018-10-24 |     always @(*)
    always @(posedge clk)
    begin
// by  2018-10-24 |         if(reset_L)
// by  2018-10-24 |         begin
            case (contador_serial)
                0: begin
                    temporal_0[1:0] <= data_in_save[1:0];
                end
                1: begin
                    temporal_1[1:0] <= data_in_save[1:0];
                end
                2: begin
                    temporal_2[1:0] <= data_in_save[1:0];
                end
                default: begin
                    temporal_3[1:0] <= data_in_save[1:0];
                end
        // by  2018-10-23 | 	    default: temporal[7:6]=data_in[1:0];
            endcase
        /*end
        else
        begin
            temporal[7:0]<=8'h0;
        end*/
    end

endmodule
