`timescale 1ns /100ps
`include "cmos_cells.v"
`include "paralelo_a_serial_estructural.v" 
module BancoPruebas_p2s();
    wire clk, valid_data_in, reset_L;
    wire [7:0] data_in;
    wire [1:0] data_out_cond, data_out_est;

    probador_p2s p0 (.clk(clk),
	    .reset_L(reset_L),
	    .data_in(data_in[7:0]),
	    .valid_data_in(valid_data_in),
	    .data_out_cond(data_out_cond[1:0]),
	    .data_out_est(data_out_est[1:0]));

    paralelo_a_serial p2s_0 (.clk(clk),
	    .reset_L(reset_L),
	    .data_in(data_in[7:0]),
	    .valid_data_in(valid_data_in),
	    .data_out(data_out_cond[1:0]));
    
    paralelo_a_serial_estructural p2s_est_0 (.clk(clk),
	    .reset_L(reset_L),
	    .data_in(data_in[7:0]),
	    .valid_data_in(valid_data_in),
	    .data_out(data_out_est[1:0]));
endmodule
